-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 05, 2018 at 10:26 PM
-- Server version: 5.5.45
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `training`
--

-- --------------------------------------------------------

--
-- Table structure for table `course_type`
--

CREATE TABLE `course_type` (
  `id` bigint(20) NOT NULL,
  `course_type_name` varchar(60) NOT NULL,
  `description` varchar(200) NOT NULL,
  `create_user` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(60) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `course_type`
--

INSERT INTO `course_type` (`id`, `course_type_name`, `description`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(1, 'sak', 'test', 'test', '2018-08-03 09:27:35', 'test', '2018-08-03 09:27:35', 0),
(2, 'mon', 'test', 'test', '2018-08-03 09:29:59', 'test', '2018-08-03 09:30:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `id` bigint(20) NOT NULL,
  `position_name` varchar(60) NOT NULL,
  `position_group` bigint(20) NOT NULL,
  `description` varchar(200) NOT NULL,
  `create_user` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(60) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`id`, `position_name`, `position_group`, `description`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(1, 'rung', 8, 'testja', 'test', '2018-08-01 09:33:55', 'test', '2018-08-01 09:33:55', 0),
(2, 'tan', 9, 'testa', 'test', '2018-08-02 06:53:33', 'test', '2018-08-02 06:57:00', 1),
(3, 'ทดสอบ', 9, 'test', 'test', '2018-08-05 21:06:51', 'test', '2018-08-05 21:06:51', 0);

-- --------------------------------------------------------

--
-- Table structure for table `position_group`
--

CREATE TABLE `position_group` (
  `id` bigint(20) NOT NULL,
  `position_group` varchar(60) NOT NULL,
  `description` varchar(200) NOT NULL,
  `create_user` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(60) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `position_group`
--

INSERT INTO `position_group` (`id`, `position_group`, `description`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(8, 'asd', 'esd', 'test', '2018-07-31 08:29:17', 'test', '2018-07-31 08:29:17', 0),
(9, 'okas', 'test3', 'test', '2018-08-01 03:59:15', 'test', '2018-08-01 03:59:15', 0),
(10, 'sak', 'test', 'test', '2018-08-01 04:14:58', 'test', '2018-08-02 07:10:05', 1),
(11, 'test', 'ddd', 'test', '2018-08-05 20:15:06', 'test', '2018-08-05 20:15:06', 0);

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `id` bigint(20) NOT NULL,
  `site_name` varchar(60) NOT NULL,
  `description` varchar(200) NOT NULL,
  `create_user` varchar(60) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_user` varchar(60) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`id`, `site_name`, `description`, `create_user`, `create_date`, `update_user`, `update_date`, `delete_flag`) VALUES
(1, 'asdr', 'hyt', 'test', '2018-07-07 05:58:29', 'test', '2018-07-07 05:58:29', 0),
(2, 'มนต์ชัย1', 'เขียนโปรแกรม', 'test', '2018-07-17 07:11:45', 'test', '2018-07-31 06:21:15', 0),
(3, 'เมาท์', 'logitech', 'test', '2018-07-17 07:13:34', 'test', '2018-07-17 07:13:34', 0),
(4, 'sd', 'test', 'test', '2018-07-26 03:04:17', 'test', '2018-07-26 03:04:17', 0),
(5, 'asd', 'test', 'test', '2018-07-26 03:11:46', 'test', '2018-07-26 03:11:46', 0),
(6, 'dede', 'test', 'test', '2018-07-29 04:42:15', 'test', '2018-07-29 04:42:15', 0),
(7, 'test', 'edit', 'test', '2018-07-29 04:53:54', 'test', '2018-07-31 06:21:06', 0),
(8, 'abc', 'test-1', 'test', '2018-07-29 04:54:10', 'test', '2018-08-02 04:50:32', 0),
(9, 'dga', 'test', 'test', '2018-07-29 04:55:29', 'test', '2018-07-29 04:55:29', 0),
(10, 'ase', 'test', 'test', '2018-07-29 05:00:54', 'test', '2018-07-29 05:00:54', 0),
(11, 'aswe', 'test', 'test', '2018-07-29 05:02:11', 'test', '2018-07-29 05:02:11', 0),
(12, 'aski', 'test', 'test', '2018-07-29 05:04:07', 'test', '2018-07-29 05:04:07', 0),
(13, 'eds', 'test', 'test', '2018-07-29 05:18:49', 'test', '2018-07-29 05:18:49', 0),
(14, 'saq', 'test', 'test', '2018-07-29 05:33:03', 'test', '2018-07-29 05:33:03', 0),
(15, 'abc', 'test', 'test', '2018-07-31 05:29:07', 'test', '2018-07-31 05:29:07', 0),
(16, 'sad', 'testa', 'test', '2018-07-31 05:54:34', 'test', '2018-07-31 05:54:34', 0),
(17, 'sdf', 'terw', 'test', '2018-07-31 05:58:52', 'test', '2018-07-31 05:58:52', 0),
(18, 'aaaa', 'zzzzz', 'test', '2018-07-31 06:13:26', 'test', '2018-08-02 04:40:24', 1),
(19, 'afc', 'test', 'test', '2018-07-31 07:45:21', 'test', '2018-07-31 07:45:21', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course_type`
--
ALTER TABLE `course_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `position_group`
--
ALTER TABLE `position_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course_type`
--
ALTER TABLE `course_type`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `position_group`
--
ALTER TABLE `position_group`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
