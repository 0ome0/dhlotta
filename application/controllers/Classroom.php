<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classroom extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		/*if(! $this->session->userdata('validated')){
            redirect('login');
        }*/
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('classroom/classroom_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addClassroom() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('ClassroomModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['class_name'] =  isset($dataPost['class_name'])?$dataPost['class_name']: 0;
			$data['class_type'] =  isset($dataPost['class_type'])?$dataPost['class_type']: "";
			$data['class_status'] = isset($dataPost['class_status'])?$dataPost['class_status']: "";
			$data['start_date'] = isset($dataPost['start_date'])?$dataPost['start_date']: "";
			$data['end_date'] =  isset($dataPost['end_date'])?$dataPost['end_date']: "";
			$data['start_time'] = isset($dataPost['start_time'])?$dataPost['start_time']: "";
			$data['end_time'] =  isset($dataPost['end_time'])?$dataPost['end_time']: "";
			$data['location'] = isset($dataPost['location'])?$dataPost['location']: "";
			$data['room'] = isset($dataPost['room'])?$dataPost['room']: "";
			$data['break_time1'] = isset($dataPost['break_time1'])?$dataPost['break_time1']: "";
			$data['break_time2'] =  isset($dataPost['break_time2'])?$dataPost['break_time2']: "";
			$data['lunch_time'] =  isset($dataPost['lunch_time'])?$dataPost['lunch_time']: "";
			$data['deadline'] =  isset($dataPost['deadline'])?$dataPost['deadline']: "";
			$data['min_trainee'] =  isset($dataPost['min_trainee'])?$dataPost['min_trainee']: "";
			$data['max_trainee'] =  isset($dataPost['max_trainee'])?$dataPost['max_trainee']: "";
			$data['introduce'] =  isset($dataPost['introduce'])?$dataPost['introduce']: "";
			$data['material'] =  isset($dataPost['material'])?$dataPost['material']: "";
			$data['pre_work'] =  isset($dataPost['pre_work'])?$dataPost['pre_work']: "";
			$data['transport'] =  isset($dataPost['transport'])?$dataPost['transport']: "";
			$data['parking'] =  isset($dataPost['parking'])?$dataPost['parking']: "";
			$data['dressing'] =  isset($dataPost['dressing'])?$dataPost['dressing']: "";
			$data['smoking'] =  isset($dataPost['smoking'])?$dataPost['smoking']: "";
			$data['source_of_cost'] =  isset($dataPost['source_of_cost'])?$dataPost['source_of_cost']: "";
			$data['budget'] =  isset($dataPost['budget'])?$dataPost['budget']: "";

			//print_r($data);
			
			//$data['update_date'] = $dateRecord;
			//$data['update_user'] = $this->session->userdata('user_name'); 
	  		// load model 
    		if ($data['id'] == 0) { 
    			//$data['delete_flag'] = 0;
				//$data['create_date'] = $dateRecord;
				//$data['create_user'] =  $this->session->userdata('user_name'); 
    			$nResult = $this->ClassroomModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->ClassroomModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteCourse(){
		try{
			$this->load->model('CourseModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->CourseModel->deleteCoursename($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getClassModelList(){
	 
		try{
			$this->load->model('ClassroomModel','',TRUE);
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
		// 	// print_r($_POST);
		// 	// print_r($this->input->post()); 
		// 	// echo $this->input->raw_input_stream;  
			
			$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->ClassroomModel->getClassModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->ClassroomModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
		// // 	//$result['message'] = $this->CourseModel->getCourseModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);				
	}
	
	
	public function getCourseModel(){
	 
		try{
			$this->load->model('CourseModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CourseModel->getCourseNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CourseModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->CourseModel->getCourseModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getCourseComboList(){
	 
		try{ 
			$this->load->model('CourseModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->CourseModel->getCourseComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
