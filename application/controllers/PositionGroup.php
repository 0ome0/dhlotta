<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PositionGroup extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		/*if(! $this->session->userdata('validated')){
            redirect('login');
        }*/
		if(! $this->session->userdata('user_name')){
            $data = array("user_name" => "test");
			$this->session->set_userdata($data);
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('site/positiongroup_view'); 
		$this->load->view('share/footer');
	}
	 
	public function addPositionGroup() {
		 
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('PositionGroupModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;			
			$data['position_group'] =  isset($dataPost['position_group'])?$dataPost['position_group']: "";
			$data['description'] =  isset($dataPost['description'])?$dataPost['description']: "";
			$data['update_user'] = $this->session->userdata('user_name');
			$data['update_date'] = date("Y-m-d H:i:s");
			 
    		if ($data['id'] == 0) {  
				$data['create_user'] = $this->session->userdata('user_name');
				$data['create_date'] = date("Y-m-d H:i:s");
    			$nResult = $this->PositionGroupModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->PositionGroupModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deletePositionGroup(){
		try{
			$this->load->model('PositionGroupModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->PositionGroupModel->deletePositionGroup($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getPositionGroupModelList(){
	 
		try{
			$this->load->model('PositionGroupModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);   	 
			
			$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";
			
			$offset = ($PageIndex - 1) * $PageSize;
			
			$result['status'] = true;
			$result['message'] = $this->PositionGroupModel->getPositionGroupList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->PositionGroupModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getPositionGroupModel(){
	 
		try{
			$this->load->model('PositionGroupModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->PositionGroupModel->getPositionGroupList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->PositionGroupModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getPositionGroupComboList(){
	 
		try{ 
			$this->load->model('PositionGroupModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->PositionGroupModel->getPositionGroupComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
