<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CourseType extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		/*if(! $this->session->userdata('validated')){
            redirect('login');
        }*/
		if(! $this->session->userdata('user_name')){
            $data = array("user_name" => "test");
			$this->session->set_userdata($data);
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('course/coursetype_view'); 
		$this->load->view('share/footer');
	}
	 
	public function addCourseType() {
		 
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('CourseTypeModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['course_type_name'] =  isset($dataPost['course_type_name'])?$dataPost['course_type_name']: "";
			$data['description'] =  isset($dataPost['description'])?$dataPost['description']: "";
			$data['update_user'] = $this->session->userdata('user_name');
			$data['update_date'] = date("Y-m-d H:i:s");
			 
    		if ($data['id'] == 0) {  
				$data['create_user'] = $this->session->userdata('user_name');
				$data['create_date'] = date("Y-m-d H:i:s");
    			$nResult = $this->CourseTypeModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->CourseTypeModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteCourseType(){
		try{
			$this->load->model('CourseTypeModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->CourseTypeModel->deleteCourseTypename($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getCourseTypeModelList(){
	 
		try{
			$this->load->model('CourseTypeModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);   
			
			$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			
			$result['status'] = true;
			$result['message'] = $this->CourseTypeModel->getCourseTypeNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CourseTypeModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getCourseTypeModel(){
	 
		try{
			$this->load->model('CourseTypeModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CourseTypeModel->getCourseTypeNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CourseTypeModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getCourseTypeComboList(){
	 
		try{ 
			$this->load->model('CourseTypeModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->CourseTypeModel->getCourseTypeComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
