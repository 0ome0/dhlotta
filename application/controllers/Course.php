<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		/*if(! $this->session->userdata('validated')){
            redirect('login');
        }*/
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('course/course_view'); 
		$this->load->view('share/footer');
	}
	
	
	public function addCourse() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('CourseModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			/*print_r($_POST);
			print_r($this->input->post()); 
			echo $this->input->raw_input_stream;*/	
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['course_name'] =  isset($dataPost['course_name'])?$dataPost['course_name']: 0;
			$data['course_type'] =  isset($dataPost['course_type'])?$dataPost['course_type']: "";
			$data['abbrivation'] = isset($dataPost['abbrivation'])?$dataPost['abbrivation']: "0";
			$data['overview'] =  isset($dataPost['overview'])?$dataPost['overview']: "";
			$data['pre_work'] =  isset($dataPost['pre_work'])?$dataPost['pre_work']: "";
			$data['duration'] =  isset($dataPost['duration'])?$dataPost['duration']: "";
			$data['course_expire'] =  isset($dataPost['course_expire'])?$dataPost['course_expire']: "";
			$data['budget'] =  isset($dataPost['budget'])?$dataPost['budget']: "";
			
			//print_r($data);
			
			//$data['update_date'] = $dateRecord;
			//$data['update_user'] = $this->session->userdata('user_name'); 
	  		// load model 
    		if ($data['id'] == 0) { 
    			//$data['delete_flag'] = 0;
				//$data['create_date'] = $dateRecord;
				//$data['create_user'] =  $this->session->userdata('user_name'); 
    			$nResult = $this->CourseModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->CourseModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteCourse(){
		try{
			$this->load->model('CourseModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->CourseModel->deleteCoursename($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	public function getCourseModelList(){
	 
		try{
			$this->load->model('CourseModel','',TRUE);
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			// print_r($_POST);
			// print_r($this->input->post()); 
			// echo $this->input->raw_input_stream;  
			
			$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CourseModel->getCourseModelList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CourseModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
		// 	//$result['message'] = $this->CourseModel->getCourseModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getCourseModel(){
	 
		try{
			$this->load->model('CourseModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			//print_r($_POST);
			//print_r($this->input->post()); 
			//echo $this->input->raw_input_stream;  
			
			//$dateRecord = date("Y-m-d H:i:s"); 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->CourseModel->getCourseNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->CourseModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			
			//$result['message'] = $this->CourseModel->getCourseModel(); 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getCourseComboList(){
	 
		try{ 
			$this->load->model('CourseModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->CourseModel->getCourseComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
