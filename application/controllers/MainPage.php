<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainPage extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		/*if(! $this->session->userdata('validated')){
            redirect('login');
        }*/
    }
	 
	public function index()
	{ 
		$this->load->view('mainpage/mainpage_view');
	}
	 
	// public function addDashborad() {
		 
		// $nResult = 0;
		
	  	// try{
	  			
	  		// $this->load->model('DashboradModel','',TRUE); 
			
			// $dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			// $data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			// $data['code'] =  isset($dataPost['code'])?$dataPost['code']: 0;
			// $data['name'] =  isset($dataPost['name'])?$dataPost['name']: "";
			// $data['contact'] = isset($dataPost['contact'])?$dataPost['contact']: "";
			// $data['address1'] = isset($dataPost['address1'])?$dataPost['address1']: "";
			// $data['address2'] =  isset($dataPost['address2'])?$dataPost['address2']: "";
			// $data['address3'] = isset($dataPost['address3'])?$dataPost['address3']: "";
			// $data['tel'] =  isset($dataPost['tel'])?$dataPost['tel']: "";
			// $data['email'] = isset($dataPost['email'])?$dataPost['email']: "";
			// $data['taxid'] = isset($dataPost['taxid'])?$dataPost['taxid']: "";
			// $data['deleteflag'] = isset($dataPost['deleteflag'])?$dataPost['deleteflag']: "0"; 
			 
    		// if ($data['id'] == 0) {  
    			// $nResult = $this->DashboradModel->insert($data);
		    // }
		    // else {  
		      	// $nResult = $this->DashboradModel->update($data['id'], $data);
		    // }
			
			// if($nResult > 0){ 
				// $result['status'] = true;
				// $result['message'] = $this->lang->line("savesuccess");
			// }else{
				// $result['status'] = false;
				// $result['message'] = $this->lang->line("error");
			// } 
			
    	// }catch(Exception $ex){
    		// $result['status'] = false;
			// $result['message'] = "exception: ".$ex;
    	// }
	    
		// echo json_encode($result, JSON_UNESCAPED_UNICODE);
    // }
	
	// public function deleteDashborad(){
		// try{
			// $this->load->model('DashboradModel','',TRUE);
			// $dataPost = json_decode( $this->input->raw_input_stream , true);
			// $id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			// $bResult = $this->DashboradModel->deleteDashboradname($id);
			 
			// if($bResult){
				// $result['status'] = true;
				// $result['message'] = $this->lang->line("savesuccess");
			// }else{
				// $result['status'] = false;
				// $result['message'] = $this->lang->line("error_faliure");
			// }
			
		// }catch(Exception $ex){
			// $result['status'] = false;
			// $result['message'] = "exception: ".$ex;
		// }
		
		// echo json_encode($result, JSON_UNESCAPED_UNICODE);
	// }
	
	// public function getDashboradModelList(){
	 
		// try{
			// $this->load->model('DashboradModel','',TRUE); 
			   	 
			
			// $result['status'] = true;
			// $result['message'] = $this->lang->line("savesuccess");
			 
		// }catch(Exception $ex){
			// $result['status'] = false;
			// $result['message'] = "exception: ".$ex;
		// }
		
		// echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	// }
	
	
	// public function getDashboradModel(){
	 
		// try{
			// $this->load->model('DashboradModel','',TRUE); 
			// $dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		// $PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			// $PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			// $direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			// $SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			// $dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			// $offset = ($PageIndex - 1) * $PageSize;
			 
			// $result['status'] = true;
			// $result['message'] = $this->DashboradModel->getDashboradNameList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			// $result['totalRecords'] = $this->DashboradModel->getTotal($dataModel);
			// $result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
			 
		// }catch(Exception $ex){
			// $result['status'] = false;
			// $result['message'] = "exception: ".$ex;
		// }
		
		// echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	// }
	
	// public function getDashboradComboList(){
	 
		// try{ 
			// $this->load->model('DashboradModel','',TRUE);
			// $result['status'] = true;
			// $result['message'] = $this->DashboradModel->getDashboradComboList();
		// }catch(Exception $ex){
			// $result['status'] = false;
			// $result['message'] = "exception: ".$ex;
		// }
		
		// echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	// }
}
