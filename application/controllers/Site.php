<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
	public function __construct() {
        parent::__construct(); 
		/*if(! $this->session->userdata('validated')){
            redirect('login');
        }*/
		if(! $this->session->userdata('user_name')){
            $data = array("user_name" => "test");
			$this->session->set_userdata($data);
        }
    }
	 
	public function index()
	{
		$this->load->view('share/head');
		$this->load->view('share/sidebar');
		$this->load->view('site/site_view'); 
		$this->load->view('share/footer');
	}
	 
	public function addSite() {
		 
		$nResult = 0;
		
	  	try{
	  			
	  		$this->load->model('SiteModel','',TRUE); 
			
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			 
			$data['id'] =  isset($dataPost['id'])?$dataPost['id']: 0;
			$data['site_name'] =  isset($dataPost['site_name'])?$dataPost['site_name']: "";
			$data['description'] =  isset($dataPost['description'])?$dataPost['description']: "";
			$data['update_user'] = $this->session->userdata('user_name');
			$data['update_date'] = date("Y-m-d H:i:s");
			
			 
    		if ($data['id'] == 0) {  
				$data['create_user'] = $this->session->userdata('user_name');
				$data['create_date'] = date("Y-m-d H:i:s");
    			$nResult = $this->SiteModel->insert($data);
		    }
		    else {  
		      	$nResult = $this->SiteModel->update($data['id'], $data);
		    }
			
			if($nResult > 0){ 
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error");
			} 
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
	
	public function deleteSite(){
		try{
			$this->load->model('SiteModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->SiteModel->deleteSitename($id);
			 
			if($bResult){
				$result['status'] = true;
				$result['message'] = $this->lang->line("savesuccess");
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
	}
	
	
	
	public function getSiteModelList(){ //client เรียก มา ที่นี่  ปรากฏว่า เรายังไม่ได้ ใส่ code 
	 
		try{
			$this->load->model('SiteModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->SiteModel->getSiteList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->SiteModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	
	public function getSiteModel(){
	 
		try{
			$this->load->model('SiteModel','',TRUE); 
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			
			 
	  		$PageIndex =  isset($dataPost['PageIndex'])?$dataPost['PageIndex']: 1;
			$PageSize =  isset($dataPost['PageSize'])?$dataPost['PageSize']: 20;
			$direction =  isset($dataPost['SortColumn'])?$dataPost['SortColumn']: "";
			$SortOrder = isset($dataPost['SortOrder'])?$dataPost['SortOrder']: "asc";
			$dataModel = isset($dataPost['mSearch'])?$dataPost['mSearch']: "";

			$offset = ($PageIndex - 1) * $PageSize;
			 
			$result['status'] = true;
			$result['message'] = $this->SiteModel->getSiteList($dataModel , $PageSize, $offset, $direction, $SortOrder );
			$result['totalRecords'] = $this->SiteModel->getTotal($dataModel);
			$result['toTalPage'] = ceil( $result['totalRecords'] / $PageSize);
			 
			 
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
	
	public function getSiteComboList(){
	 
		try{ 
			$this->load->model('SiteModel','',TRUE);
			$result['status'] = true;
			$result['message'] = $this->SiteModel->getSiteComboList();
		}catch(Exception $ex){
			$result['status'] = false;
			$result['message'] = "exception: ".$ex;
		}
		
		echo json_encode($result, JSON_UNESCAPED_UNICODE);		
	}
}
