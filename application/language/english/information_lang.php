<?php	
$lang['welcome_message'] = 'Type message in German';
$lang['Dashboard'] = 'Dashboard';
$lang['Dormitory'] = 'Dormitory';
$lang['Rooms'] = 'Rooms';
$lang['Customer'] = 'Customer';
$lang['Invoice'] = 'Invoice';
$lang['MaCost'] = 'MA Cost';
$lang['Report'] = 'Report';
$lang['DormitoryName'] = 'Dormitory Name';
$lang['AeDormitory'] = 'Ae Dormitory';
$lang['Branch'] = 'Branch';
$lang['Address'] = 'Address';
$lang['Save'] = 'Save';
$lang['Cancel'] = 'Cancel';
$lang['Edit'] = 'Edit';
$lang['Delete'] = 'Delete';
$lang['Contact'] = 'Contact';
$lang['Email'] = 'Email';
$lang['Tel'] = 'Tel';
$lang['Fax'] = 'Fax';
$lang['DormitoryList'] = 'Dormitory List';
$lang['Option'] = 'Option';
$lang['RoomType'] = 'Room Type';
$lang['RoomItem'] = 'Room Item';
$lang['RoomInDorm'] = 'Room In Dorm';
$lang['ViewDatail'] = 'View Details';
$lang['RoomTypeCode'] = 'Type Code';
$lang['RoomTypeName'] = 'Type Name';
$lang['RoomPrice'] = 'Prices';
$lang['Note'] = 'Note'; 
$lang['RoomTypeList'] = 'Room Type List';
$lang['RoomItemCode'] = 'Item Code';
$lang['RoomItemName'] = 'Item Name';
$lang['ItemPrice'] = 'Prices';
$lang['RoomItemList'] = 'Room Item List';
$lang['PleaseSelectDorm'] = 'Please select dormitory';
$lang['Next'] = 'Next';
$lang['RoomNumber'] = 'Room Number';
$lang['RoomFloor'] = 'Floor';
$lang['RoomInDormList'] = 'Room in Dormitory List';
$lang['CustomerName'] = 'Name';
$lang['CustomerSurName'] = 'SurName';
$lang['CustomerIDCard'] = 'ID Card';
$lang['CustomerAddress1'] = 'Address 1';
$lang['CustomerAddress2'] = 'Address 2';
$lang['Picture'] = 'Picture';
$lang['Deposit'] = 'Deposit';
$lang['NameSurName'] = 'Name & Surname';
$lang['Day'] = 'Day';
$lang['Week'] = 'Week';
$lang['Month'] = 'Month';
$lang['Year'] = 'Year';
$lang['Booking'] = 'Booking';
$lang['CheckInDate'] = 'Check In';
$lang['CheckOutDate'] = 'Check Out';
$lang['CustomerType'] = 'Customer Type'; 
$lang['DepositMonth'] = 'DepositMonth';
$lang['OldCustomer'] = 'Old customer'; 
$lang['NewCustomer'] = 'New customer';
$lang['FileInput'] = 'File input';
$lang['PictureIDCard'] = 'PictureIDCard';
$lang['Price'] = 'Price';
$lang['ContactDate'] = 'Contact Date';
$lang['ContactPerson'] = 'Contact Person';
$lang['Relatetion'] = 'Relatetion';
$lang['QtyPerson'] = 'Qty Person';
$lang['OptionNote'] = 'Option Note';
$lang['DisCountNote'] = 'DisCount Note';
$lang['Insurrance'] = 'Insurrance';
$lang['RoomStatus'] = 'Status';
$lang['RoomManage'] = 'Manage';
$lang['RoomAll'] = 'All';
$lang['RoomFull'] = 'Room Full';
$lang['RoomEmpty'] = 'Room Empty';
$lang['Vat'] = 'Vat';
$lang['Quatation'] = 'Quatation';
$lang['Invoice'] = 'Invoice';
$lang['System'] = 'System';
$lang['Company'] = 'Company';
$lang['CompanyAddress'] = 'Company Address';
$lang['CompanyID'] = 'Company ID (13 digit)';
$lang['CompanyID2'] = 'Company ID';
$lang['CusCompany'] = 'Customer Company';
$lang['CusCompanyAddress'] = 'Customer Company Address';
$lang['CusCompanyID'] = 'Customer Company ID (13 digit)';
$lang['CusCompanyID2'] = 'Customer Company ID';
$lang['VatBook'] = 'Vat Book';
$lang['BookNo'] =  'Book No.';
$lang['NumNo'] =  'Num No.';
$lang['CompanyNameEx'] =  'ผู้มีหน้าที่หักภาษีณ ที่จ่าย';
$lang['CompanyNameEx2'] =  'ผู้ถูกหักภาษีณ ที่จ่าย';
$lang['withHoldingTax'] =  'ภาษีที่หักและนำส่งไว้';
$lang['PayPrices'] =  'จำนวนเงินที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['PayDate'] =  'วันเดือนหรือปีภาษีที่จ่าย';
$lang['ListOfWHT'] =  'รายการภาษีหัก ณ ที่จ่าย';
$lang['Date'] =  'วันที่';
$lang['DueDate'] =  'Due Date';
$lang['CustomerID'] =  'Customer ID';
$lang['InvoiceRef'] =  'Invoice Ref';
$lang['DateRef'] =  'Date Ref';
$lang['Company'] =  'Company';
$lang['CompanyDetail'] =  'Company Detail';
$lang['Contact'] =  'Contact';
$lang['Address1'] =  'Address1';
$lang['Address2'] =  'Address2';
$lang['Address3'] =  'Address3';
$lang['taxid'] =  'taxid';
$lang['website'] =  'website';
$lang['savesuccess'] = 'save success';
$lang['error'] = 'error';
$lang['Project'] = 'Project';
$lang['CompanyCode'] = 'Company Code';
$lang['ListOfCustomer'] = 'List Of Customer';
$lang['DoYouWantToDelete'] = 'Do you want to delete ?';
$lang['NoCancel'] = 'No';
$lang['Yes'] = 'Yes';
$lang['Close'] = 'Close';
$lang['Confirmation'] = 'Confirmation';
$lang['Information'] = 'Information';
$lang['Search'] = 'Search';
$lang['ResetSearch'] = 'Reset Search';
$lang['Search'] = 'Search';
$lang['Add'] = 'Add';
$lang['Total'] = 'Total';
$lang['Records'] = 'Records';
$lang['ResultsPerPage'] = 'ResultsPerPage';
$lang['Previous'] = 'Previous';
$lang['Next'] = 'Next';
$lang['ProjectName'] = 'Project Name';
$lang['budget'] = 'Budget';
$lang['note'] = 'Note';
$lang['projectyear'] = 'Project Year';
$lang['ListOfProject'] = 'List of Project';
$lang['Require'] = 'Require';
$lang['Unit'] = 'Unit';
$lang['DESCRIPTION'] = 'DESCRIPTION';
$lang['Quantity'] = 'Qty';
$lang['Price'] = 'Price';
$lang['Amount'] = 'Amount';
$lang['ITEM'] = 'ITEM';
$lang['SubTotal'] = 'Sub Total';
$lang['VAT'] = 'VAT 7%';
$lang['Total'] = 'Total';
$lang['sub_alphabet'] = 'รวมเงินภาษีที่หักนำส่ง (ตัวอักษร) ';
$lang['SignName'] = 'ลงชื่อผู้จ่ายเงิน';
$lang['SignDate'] = 'วัน เดือน ปี ที่ออกหนังสือรับรอง';
$lang['ListOfQuatation'] = 'List of quatation';
$lang['Print'] = 'Print';
$lang['PrintAll'] = 'Print All';
$lang['Payment'] = 'Perment term and condition';
$lang['SocialFund'] = 'Social Fund';
$lang['Fund'] = 'Fund';
$lang['Course'] = 'Course';
$lang['Class'] = 'Class';
$lang['Site'] = 'Service Center';
$lang['Config'] = 'Config';
$lang['Trainer'] = 'Trainer';
$lang['Trainee'] = 'Trainee';
$lang['Position'] = 'Position';
$lang['CategoryPosition'] = 'Category Position';
$lang['CourseType'] = 'Course Type';
$lang['OTAA'] = 'OTAA:Operations Training Administrator Application';
$lang['CourseType'] =  'Course Type';
$lang['ManageCourse'] =  'Manage Course';
$lang['ManageCourseType'] =  'Manage Course Type';
$lang['ManageSite'] =  'Manage Site';
$lang['Site'] =  'Service Center';
$lang['PositionGroup'] =  'Position Group';
$lang['Position'] =  'Position';
$lang['MapPositionToCause'] =  'Map Position To Course';
$lang['ManageTrainee'] =  'Manage Trainee';
$lang['ManageTrainer'] =  'Manage Trainer';
$lang['AssignTrainee'] = 'Assign Trainee';
$lang['RegisterForm']  = 'Register Form';
$lang['AttendanceList']  = 'Attendance List';
$lang['ManageClass'] = 'Manage Class';
$lang['PrintCertificate'] = 'Print Certificate';
$lang['Certificate'] = 'Certificate';
$lang['Evaluation'] = 'Evaluate Follow Up';
$lang['Evaluated'] = 'Evaluated';
$lang['ListOfSite'] = 'List Of Site';
$lang['Description'] = 'Description';
$lang['SiteCode'] = 'Service Center  Code';
$lang['SiteName'] = 'Service Center  Name';
$lang['PositionGroupName'] = 'Position Group';
$lang['ListOfPosition'] = 'List of Position';
$lang['ListOfPositionGroup'] = 'List of Position Group';
$lang['ListOfMapPosition'] = 'List of Position to Course';
$lang['GroupPositionName'] = 'Position Group Name';
$lang['PositionName'] = 'Position Name';
$lang['CourseTypeName'] = 'Course Type Name';
$lang['ListOfCourse'] = 'List of Course';
$lang['ListOfCourseType'] = 'List of Course Type';
$lang['CourseName'] = 'Course Name';
$lang['CourseCode'] = 'Course Code';
$lang['Confirm'] = 'Confirm';
$lang['MinimumTrainee'] = 'Minimum Trainee';
$lang['MaximumTrainee'] = 'Maximum Trainee';
$lang['Abbrivation'] = 'Abbrivation';
$lang['Prework'] = 'Pre-Work';
$lang['Overview'] = 'Overview';
$lang['Duration'] = 'Duration';
$lang['Minute'] = 'Minute';
$lang['Material'] = 'Material';
$lang['RequireCourse'] = 'Require Course';
$lang['Information'] = 'Information'; 
$lang['TrainerList'] = 'Trainer List'; 
$lang['MaterialList'] = 'Material List';
$lang['RequireCourseList'] = 'Pre Requisition';
$lang['TrainerName'] = 'Trainer Name';
$lang['MaterialName'] = 'Material Name';
$lang['Download'] = 'Download';
$lang['ListofClass'] = 'List of Class';
$lang['ClassCode'] = 'Class Code';
$lang['ClassStatus'] = 'Class Status';
$lang['StartTime'] = 'Start Time';
$lang['EndTime'] = 'End Time';
$lang['OutlineIntroduce'] = 'Outline Introduce';
$lang['MaterialAcquired'] = 'Material Acquired';
$lang['Location'] = 'Location';
$lang['Room'] = 'Room';
$lang['Deadline'] = 'Deadline self Register';
$lang['StudentNo'] = 'StudentNo';
$lang['ClassType'] = 'ClassType';
$lang['BreakTime1'] = 'BreakTime1';
$lang['LunchTime'] = 'LunchTime';
$lang['BreakTime2'] = 'BreakTime2';
$lang['ClassMap'] = 'Map';
$lang['TransportationPolicy'] = 'Transportation Policy';
$lang['ParkingPolicy'] = 'Parking Policy';
$lang['DressingPolicy'] = 'Dressing Policy';
$lang['SmokingPolicy'] = 'Smoking Policy';
$lang['SourceOfCost'] = 'Source Of Cost';
$lang['BudgetCost'] = 'Budget Of Cost';
$lang['StartDate'] = 'Start Date';
$lang['EndDate'] = 'End Date';
$lang['Upload'] = 'Upload';
$lang['CourseAndTrainer'] = 'Course & Trainer';
$lang['ClassName'] = 'Class Name';
$lang['Manage'] = 'Manage';
$lang['Mapping'] = 'Mapping';
$lang['TraineeName'] = 'Trainee Name';
$lang['TraineeList'] = 'Trainee List';
$lang['Assign'] = 'Assign';
$lang['ListOfTrainee'] = 'List Of Trainee';
$lang['TraineeCode'] = 'Trainee Code';
$lang['Password'] = 'Password';
$lang['ListOfAttendance'] = 'List Of Attendance'; 
$lang['AttendanceFile'] = 'Please select attendance PDF file';
$lang['ListOfCertificate'] = 'List of Certificate';
$lang['ListOfEvaluation'] = 'List of Evaluation';
$lang['Comment'] = 'Comment';
$lang['ExportFile'] = 'Export File';
$lang['UserManagement'] = 'User Management';
$lang['UserName'] = 'User Name';
$lang['ListOfUserManagement'] = 'List Of UserManagement';
$lang['ManageTrainee'] = 'Manage Trainee';
$lang['CourseSummary'] = 'Course Summary';
$lang['ListOfCourseSummary'] = 'List Of Course Summary';
$lang['CertificateDate'] = 'Certificate Date';
$lang['ExpireDate'] = 'Expire Date';
$lang['ConfirmClass'] = 'Confirm Class';
$lang['ListofConfirmClass'] = 'List of Confirm Class';
$lang['Confirm'] = 'Confirm';
$lang['ConfirmTrainee'] = 'Confirm Trainee';
$lang['ListofConfirmTrainee'] = 'List of Confirm Trainee';;
$lang['Reject'] = 'Reject';
$lang['Wait'] = 'Wait';
$lang['PassStatus'] = 'Pass Status';
$lang['CourseExpire'] = 'Reoccurred';
$lang['Month'] = 'Months';
$lang['SendMail'] = 'Send Email Status';
$lang['ConfirmBy'] = 'Confirm By';
$lang['Employee'] = 'Employee';
$lang['Score'] = 'Score';
$lang['Remark'] = 'Remark';
$lang['Facilitator'] = 'Facilitator';
$lang['EmployeeCode'] = 'Employee Number';
$lang['EmployeeName'] = 'Employee Name';
$lang['EmployeeLName'] = 'Employee Lastname';
$lang['ListOfEmployee'] = 'List Of Employee';
$lang['ForgetPassword'] = 'Forget Password';
$lang['SelectAll'] = 'Select All';
$lang['Complete'] = 'Complete';
$lang['VerifyBy'] = 'Verify By';
$lang['ConfirmStatus'] = 'Confirm Status';
$lang['ExcuteClass'] = 'Excute Class';
$lang['Excute'] = 'Excute';

