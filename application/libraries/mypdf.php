<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require('fpdf.php');

define('FPDF_FONTPATH',APPPATH .'fpdf/font/');
require(APPPATH .'fpdf/fpdf.php');

class  mypdf extends FPDF
{
	function __construct($orientation='P', $unit='mm', $size='A4')
	{
		parent::__construct($orientation,$unit,$size);
	}
}
?>