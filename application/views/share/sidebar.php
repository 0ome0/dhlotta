<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.html"><?php echo $this->lang->line('OTAA');?></a>
	</div>
	<!-- /.navbar-header -->
	
	 
	<ul class="nav navbar-top-links navbar-right"> 
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="<?php echo base_url('/Login');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>
			</ul>
			<!-- /.dropdown-user -->
		</li>
		<!-- /.dropdown -->
	</ul>
	  
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu"> 
				<?php /*<li>
					<a href="<?php echo base_url('/Dashboard');?>"><i class="fa fa-dashboard fa-fw"></i> <?php echo $this->lang->line('Dashboard');?></a>
				</li>*/ ?>
				<li  >
					<a href="#"><i class="fa fa-sitemap "></i> <?php echo $this->lang->line('ManageSite');?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  "  >
						<li>
							<a href="<?php echo base_url('/Site');?>"><?php echo $this->lang->line('Site');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/PositionGroup');?>"><?php echo $this->lang->line('PositionGroup');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Position');?>"><?php echo $this->lang->line('Position');?></a>
						</li> 
						<li>
							<a href="<?php echo base_url('/PositionMap');?>"><?php echo $this->lang->line('MapPositionToCause');?></a>
						</li> 
					</ul>
				</li>
				<li  >
					<a href="#"><i class="fa fa-book "></i> <?php echo $this->lang->line('ManageCourse');?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  "   >
						<li>
							<a  href="<?php echo base_url('/CourseType');?>"><?php echo $this->lang->line('CourseType');?></a>
						</li>
						<li>
							<a  href="<?php echo base_url('/Course');?>"><?php echo $this->lang->line('Course');?></a>
						</li>
					</ul> 
				</li> 
				<li>
					<a href="#"><i class="fa fa-calendar "></i> <?php echo $this->lang->line('ManageClass');?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  "   >
						<li>
							<a  href="<?php echo base_url('/Classroom');?>"><?php echo $this->lang->line('Class');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/AssignTrainee');?>"><?php echo $this->lang->line('AssignTrainee');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/ConfirmTrainee');?>"><?php echo $this->lang->line('ConfirmTrainee');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/ConfirmClass');?>"><?php echo $this->lang->line('ExcuteClass');?></a>
						</li>
					</ul> 
				</li>   
				<li>
					<a href="#"><i class="glyphicon glyphicon-list-alt"></i> <?php echo $this->lang->line('Facilitator');?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  "   >
						<li>
							<a href="<?php echo base_url('/RegisterForm');?>"><?php echo $this->lang->line('RegisterForm');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/Attendance');?>"><?php echo $this->lang->line('AttendanceList');?></a>
						</li>				
						<li>
							<a href="<?php echo base_url('/Certificate');?>"><?php echo $this->lang->line('Certificate');?></a>
						</li>
					</ul>  
				</li>
				<!-- <li>
					<a href="<?php echo base_url('/Evaluation');?>"><i class="	glyphicon glyphicon-star "></i> <?php echo $this->lang->line('Evaluation');?></a> 
				</li> -->
				<li>
					<a href="#"><i class="	glyphicon glyphicon-star "></i> <?php echo $this->lang->line('Report');?><span class="fa arrow"></span></a> 
				</li>
				<li> 
					<a href="#"><i class="fa fa-bar-chart "></i> <?php echo $this->lang->line('Trainee');?><span class="fa arrow"></span></a>
					<ul class="nav nav-second-level  "   >
						<li>
							<a href="<?php echo base_url('/Trainee');?>"><?php echo $this->lang->line('Trainee');?></a>
						</li>
						<li>
							<a href="<?php echo base_url('/CourseSummary');?>"><?php echo $this->lang->line('CourseSummary');?></a>
						</li>
					</ul>
				</li>
				<li>
					<a href="<?php echo base_url('/UserManage');?>"><i class="fa fa-user fa-fw"></i> <?php echo $this->lang->line('UserManagement');?></a>
				</li> 
				<?php /*<li>
					<a href="<?php echo base_url('/Config');?>"><i class="fa fa-dashboard fa-fw"></i> <?php echo $this->lang->line('Config');?></a>
				</li> */ ?>
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>


<div id="page-wrapper">
