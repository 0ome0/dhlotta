<script src="<?php echo base_url('asset/customerController.js');?>"></script>
 <div  ng-controller="customerController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator"> 
		<li class="nav_active"> <?php echo $this->lang->line('Trainee');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Trainee');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12"> 
								<label><?php echo $this->lang->line('SiteName');?></label>
								<select class="form-control">
									<option ></option>
									<option >APD</option>
									<option >CXM</option>
									<option >EGW</option> 
								</select>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('TraineeCode');?></label>
								<input class="form-control" ng-model="CreateModel.code" maxlength="80" > 
							</div>  
						</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('PositionGroup');?></label>
								<select class="form-control">
									<option ></option>
									<option >Manager</option>
									<option >Ground Ops</option>
									<option >Local Ops</option> 
									<option >Night Shift Ops</option> 
									<option >Other Functions</option> 
								</select> 
							</div><div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('PositionName');?></label>
								<select class="form-control">
									<option ></option>
									<option >Service Center Manager</option>
									<option >Courier</option>
									<option >Teamleader</option>
								</select>
							</div> 
						</div> 
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('TraineeName');?></label>
								<input class="form-control" ng-model="CreateModel.code" maxlength="80" > 
							</div><div class="col-lg-6 col-md-6 col-xs-12">
								 
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Trainee');?>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-xs-12">
									<div role="form">
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12"> 
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('SiteName');?></label>
												<select class="form-control">
													<option ></option>
													<option >APD</option>
													<option >CXM</option>
													<option >EGW</option> 
												</select>
											</div>
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('TraineeCode');?></label>
												<input class="form-control" ng-model="CreateModel.code" maxlength="80" >
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div>  
										</div> 
										<div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('TraineeName');?></label>
												<input class="form-control" ng-model="CreateModel.code" maxlength="80" >
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div><div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Password');?></label>
												<input class="form-control" type="password" ng-model="CreateModel.Password" maxlength="80" >
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PositionGroup');?></label>
												<select class="form-control">
													<option ></option>
													<option >Manager</option>
													<option >Ground Ops</option>
													<option >Local Ops</option> 
													<option >Night Shift Ops</option> 
													<option >Other Functions</option> 
												</select>
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div><div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('PositionName');?></label>
												<select class="form-control">
													<option ></option>
													<option >Service Center Manager</option>
													<option >Courier</option>
													<option >Teamleader</option>
												</select>
											</div> 
										</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
											<div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Email');?></label>
												<input class="form-control" ng-model="CreateModel.code" maxlength="80" >
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											</div><div class="col-lg-6 col-md-6 col-xs-12">
												<label><span class="text-danger" >*</span><?php echo $this->lang->line('Note');?></label>
												 <textarea class="form-control" rows="3"></textarea>
												<p class="CreateModel_code require text-danger"><?php echo $this->lang->line('Require');?></p>
											 
											</div> 
										</div>
										
										<div class="form-group text-right">
											<br/><br/>
											<div class="col-lg-12 col-md-12 col-xs-12">
											<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
											<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
											</div>
										</div>
										
										<div class="row text-primary  " style="font-size:xx-small;" >
											<div class="col-md-6 col-xs-12 timestampshow text-left">
												Create by Monchai LapphoOlarn (08-08-2018 00:00:00)
											</div>
											<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
												Update by  Monchai LapphoOlarn (08-08-2018 12:30:00)
											</div>
										</div>
									</div>
								</div>  
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfTrainee');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('SiteName');?></th>
											<th><?php echo $this->lang->line('TraineeCode');?></th>
											<th><?php echo $this->lang->line('TraineeName');?></th>
											<th><?php echo $this->lang->line('PositionGroup');?></th>
											<th><?php echo $this->lang->line('Position');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr >
											<td>APD</td>
											<td>M001</td>
											<td>Mr.Akaderk Sailim</td>
											<td>Manager</td>
											<td>Service Center Manager</td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr><tr >
											<td>APD</td>
											<td>C001</td>
											<td>Mr.Somchai Sandee</td>
											<td>Ground Ops</td>
											<td>Courier</td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr><tr >
											<td>CXM</td>
											<td>C002</td>
											<td>Mr.Suparak Laopang</td>
											<td>Ground Ops</td>
											<td>Courier</td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
										<?php /*<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.code"></td> 
                                            <td ng-bind="item.name"></td>
                                            <td ng-bind="item.contact"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> */ ?>
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>