<script src="<?php echo base_url('asset/classroomController.js');?>"></script>
 <div  ng-controller="classroomController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<li class="nav_active"> <?php echo $this->lang->line('ManageClass');?> --> </li>
		<li class="nav_active"> <?php echo $this->lang->line('Class');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Class');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('ClassName');?></label>
								<input class="form-control" ng-model="modelSearch.code" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('Location');?></label>
								<input class="form-control" ng-model="modelSearch.name" maxlength="80" >
								<p class="help-block"></p>
							</div> 
						</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('ClassStatus');?></label>
								<select class="form-control">
									<option ></option>
									<option >PENDING</option>
									<option >OPEN</option>
									<option >CLOSE</option>
									<option >COMPLETED</option> 
								</select>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								 
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Class');?>
						</div>
						<div class="panel-body">
							
							<div class="col-lg-12 col-md-12 col-xs-12">
							 <ul class="nav nav-tabs">
								<li class="active"><a href="#Information"><?php echo $this->lang->line('Information');?></a></li>
								<li><a href="#CourseAndTrainer"><?php echo $this->lang->line('CourseAndTrainer');?></a></li>
								 
							  </ul>

							  <div class="tab-content">
								<div id="Information" class="tab-pane fade in active">
								  <div class="row">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<br/>
										<div role="form">
											<div class="form-group col-lg-12 col-md-12 col-xs-12">	
												<div class="col-lg-9 col-md-9 col-xs-6"> 
													 &nbsp;
												</div><div class="col-lg-3 col-md-3 col-xs-6">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('ClassCode');?></label>
													<input class="form-control" ng-model="CreateModel.id"  maxlength="80">
													<p class="CreateModel_id require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div>
											</div>
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-6 col-md-6 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('ClassName');?></label>
													<input class="form-control" ng-model="CreateModel.class_name" maxlength="80" >
													<p class="CreateModel_class require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div><div class="col-lg-3 col-md-3 col-xs-6"> 
													<label><?php echo $this->lang->line('ClassType');?></label>
													<select class="form-control">
														<option ></option>
														<option >Internal</option>
														<option >External</option> 
													</select>
												</div><div class="col-lg-3 col-md-3 col-xs-6"> 
													<label><?php echo $this->lang->line('ClassStatus');?></label>
													<select class="form-control">
														<option ></option>
														<option >PENDING</option>
														<option >OPEN</option>
														<option >CLOSE</option>
														<option >COMPLETED</option> 
													</select>
												</div>  
											</div> 
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('StartDate');?></label>
													<input class="form-control" ng-model="CreateModel.start_date" data-date-format="dd-MMM-yyyy"  bs-datepicker>
													<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div><div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('StartTime');?></label>
													<input class="form-control" ng-model="CreateModel.start_time" data-date-format="HH:mm:ss" bs-timepicker>
													<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div><div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('EndDate');?></label>
													<input class="form-control" ng-model="CreateModel.end_date" data-date-format="dd-MMM-yyyy" bs-datepicker>
													<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div><div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('EndTime');?></label>
													<input class="form-control" ng-model="CreateModel.end_time" data-date-format="HH:mm:ss" bs-timepicker>
													<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div>
											</div>
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-6 col-md-6 col-xs-12">
													<label><?php echo $this->lang->line('ClassMap');?></label><br/>
													<span style="text-align:center"> 
													 <img src="<?php echo base_url('/theme/image/map_dhl.png');?>" class="img-fluid"  style="  margin-bottom: 10px; width: 100%;"  /><br/>
													</span>
													 <input class="form-control" ng-model="CreateModel.code" type="file" >
													 <br/>
													 <button type="button" class="btn btn-primary waves-effect waves-light m-b-5"  ><i class="glyphicon glyphicon-upload"></i> <span class="hidden-xs"><?php echo $this->lang->line('Upload');?></span></button> 
												</div>
												<div class="col-lg-6 col-md-6 col-xs-12">
													<div class="col-lg-6 col-md-6 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('Location');?></label>
														<input class="form-control" ng-model="CreateModel.location" maxlength="80" >
														<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-6 col-md-6 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('Room');?></label>
														<input class="form-control" ng-model="CreateModel.room" maxlength="80" >
														<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-12 col-md-12 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('BreakTime1');?></label>
														<input  class="form-control" ng-model="CreateModel.break_time1" data-date-format="HH:mm:ss" bs-timepicker>
														<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-12 col-md-12 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('BreakTime2');?></label>
														<input class="form-control" ng-model="CreateModel.break_time2"  data-date-format="HH:mm:ss" bs-timepicker>
														<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-12 col-md-12 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('LunchTime');?></label>
														<input  class="form-control"   ng-model="CreateModel.lunch_time"  data-date-format="HH:mm:ss" bs-timepicker >
														<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-12 col-md-12 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('Deadline');?></label>
														<input class="form-control" ng-model="CreateModel.deadline" data-date-format="dd-MMM-yyyy" bs-datepicker>
														<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-6 col-md-6 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('MinimumTrainee');?></label>
														<input class="form-control" ng-model="CreateModel.min_trainee" maxlength="80" >
														<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div><div class="col-lg-6 col-md-6 col-xs-12">
														<label><span class="text-danger" >*</span><?php echo $this->lang->line('MaximumTrainee');?></label>
														<input class="form-control" ng-model="CreateModel.max_trainee" maxlength="80" >
														<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
													</div>
												</div> 
											</div> 
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-3 col-md-3 col-xs-3">
													
												</div><div class="col-lg-3 col-md-3 col-xs-3">
													
												</div><div class="col-lg-3 col-md-3 col-xs-3">
													
												</div><div class="col-lg-3 col-md-3 col-xs-3">
													
												</div>
											</div>
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-12 col-md-12 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('OutlineIntroduce');?></label>
													<textarea class="form-control" rows="3" ng-model="CreateModel.introduce"  maxlength="80"></textarea>
													<p class="CreateModel_address3 require text-danger"><?php echo $this->lang->line('Require');?></p>
													<br/>													
												</div> 
											</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-12 col-md-12 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('MaterialAcquired');?></label>
													<textarea class="form-control" rows="3" ng-model="CreateModel.material"  maxlength="80"></textarea> 
													<p class="CreateModel_address1 require text-danger"><?php echo $this->lang->line('Require');?></p>
													<br/>
												</div> 
											</div> 
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-12 col-md-12 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Prework');?></label>
													<textarea class="form-control" rows="3" ng-model="CreateModel.pre_work"  maxlength="80"></textarea> 
													<p class="CreateModel_address1 require text-danger"><?php echo $this->lang->line('Require');?></p>
													<br/>
												</div> 
											</div> 
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('TransportationPolicy');?></label>
													<!-- <input class="form-control" ng-model="CreateModel.name" maxlength="80" > -->
													<select class="form-control">
														<option ></option>
														<option >BTS</option>
														<option >MRT</option>
														<option >TAXI</option>
													</select>
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('ParkingPolicy');?></label>
													<!-- <input class="form-control" ng-model="CreateModel.name" maxlength="80" > -->
													<select class="form-control">
														<option ></option>
														<option >YES</option>
														<option >NO</option>
													</select>
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('DressingPolicy');?> (<?php echo $this->lang->line('Minute');?>)</label>
													<!-- <input class="form-control" ng-model="CreateModel.name" maxlength="80" > -->
													<select class="form-control">
														<option ></option>
														<option >Formal</option>
														<option >Informal</option>
													</select>
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p> 
												</div>
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label>&nbsp;<br/></label>
													<label><input type="checkbox" ng-model="CreateModel.SmokingPolicy" maxlength="80" ><?php echo $this->lang->line('SmokingPolicy');?></label>
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p> 
												</div> 
											</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('SourceOfCost');?></label>
													<input class="form-control" ng-model="CreateModel.source_of_cost" maxlength="80" >
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
												<div class="col-lg-3 col-md-3 col-xs-3">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('BudgetCost');?></label>
													<input class="form-control" ng-model="CreateModel.budget" maxlength="80" >
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div>
											</div>

											<div class="form-group text-right">
												<br/><br/>
												<div class="col-lg-12 col-md-12 col-xs-12">
												<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
												<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
												</div>
											</div> 
										</div>
									</div>  
								</div>
								</div>
								<div id="CourseAndTrainer" class="tab-pane fade">
								  <br/>
								  <button  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"  ><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button>
								  <div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('CourseName');?></th>
													<th><?php echo $this->lang->line('Trainer');?></th>
													<th><?php echo $this->lang->line('StartTime');?></th>
													<th><?php echo $this->lang->line('EndTime');?></th>
													<th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody> 
												<tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.id"></td> 
													<td ng-bind="item.class_name"></td>
													<td ng-bind="item.contact"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
								</div>
								 
							  </div>
							  
							  	<div class="row text-primary  " style="font-size:xx-small;" >
									<div class="col-md-6 col-xs-12 timestampshow text-left">
										Create by Monchai LapphoOlarn (08-08-2018 00:00:00)
									</div>
									<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
										Update by  Monchai LapphoOlarn (08-08-2018 12:30:00)
									</div>
								</div>
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListofClass');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('ClassCode');?></th>
											<th><?php echo $this->lang->line('ClassName');?></th>
											<th><?php echo $this->lang->line('StartDate');?></th>
											<th><?php echo $this->lang->line('EndDate');?></th>
											<th><?php echo $this->lang->line('Location');?></th>
											<th><?php echo $this->lang->line('Deadline');?></th>
											<th><?php echo $this->lang->line('ClassStatus');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.id"></td> 
                                            <td ng-bind="item.class_name"></td>
                                            <td ng-bind="item.start_date | date : 'dd MMM yyyy'"></td> 
											<td ng-bind="item.end_date | date : 'dd MMM yyyy'"></td> 
											<td ng-bind="item.location"></td> 
											<td ng-bind="item.deadline | date : 'dd MMM yyyy'"></td> 
											<td ng-bind="item.class_status"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('CourseAndTrainer');?></h4>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			<div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('CourseName');?></label>
					<select class="form-control">
						<option ></option>
						<option >CIS - Welcome to My Company</option>
						<option >CIS - Welcome to My Country</option>
						<option >First Step</option> 
					</select>
				</div><div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Trainer');?></label>
					<select class="form-control">
						<option ></option>
						<option >Monchai LapphoOlarn</option>
						<option >Montri LapphoOlarn</option>
						<option >Pornchai LapphoOlarn</option> 
					</select>
				</div>
			</div>
			<!-- <div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-6"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('MinimumTrainee');?></label>
					 <input class="form-control" />
				</div><div class="col-lg-6 col-md-6 col-xs-6"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('MaximumTrainee');?></label>
					<input class="form-control" />
				</div> 
			</div> -->
			<div class="row"> 
				<div class="col-lg-3 col-md-3 col-xs-3">
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('StartDate');?></label>
					<input class="form-control" ng-model="CreateModel.StartDate" data-date-format="dd-mm-yyyy" bs-datepicker>
					<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
				</div><div class="col-lg-3 col-md-3 col-xs-3">
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('StartTime');?></label>
					<input class="form-control" ng-model="CreateModel.StartTime" data-date-format="HH:mm:ss" bs-timepicker>
					<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
				</div><div class="col-lg-3 col-md-3 col-xs-3">
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('EndDate');?></label>
					<input class="form-control" ng-model="CreateModel.EndDate" data-date-format="dd-mm-yyyy" bs-datepicker>
					<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
				</div><div class="col-lg-3 col-md-3 col-xs-3">
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('EndTime');?></label>
					<input class="form-control" ng-model="CreateModel.EndTime" data-date-format="HH:mm:ss" bs-timepicker>
					<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
				</div>
			</div><div class="row"> 
				<div class="col-lg-9 col-md-9 col-xs-9">
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Note');?></label>
					<textarea class="form-control" rows="3" ng-model="CreateModel.Note" >
					</textarea> 
					<p class="CreateModel_IssueDate require text-danger"><?php echo $this->lang->line('Require');?></p>
				</div>
				<div class="col-lg-3 col-md-3 col-xs-3"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Duration');?>(<?php echo $this->lang->line('Minute');?>)</label>
					<input class="form-control" />
				</div>
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>
<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
});
// angular.module('defulttime', [])
//    .controller('classroom', ['$scope', function($scope) {
//      $scope.time = {
// 	   BreakTime1: new Date(1970, 0, 1,10, 0, 0),
// 	   BreakTime2: new Date(1970, 0, 1,14, 0, 0)
//      };
//    }]);
</script>
       