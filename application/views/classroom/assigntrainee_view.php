<script src="<?php echo base_url('asset/customerController.js');?>"></script>
 <div  ng-controller="customerController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<li class="nav_active"> <?php echo $this->lang->line('ManageClass');?> --> </li>
		<li class="nav_active"> <?php echo $this->lang->line('AssignTrainee');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('AssignTrainee');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('ClassName');?></label>
								<input class="form-control" ng-model="modelSearch.code" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('Location');?></label>
								<input class="form-control" ng-model="modelSearch.name" maxlength="80" >
								<p class="help-block"></p>
							</div> 
						</div><div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('ClassStatus');?></label>
								<select class="form-control">
									<option ></option>
									<option >PENDING</option>
									<option >OPEN</option>
									<option >CLOSE</option>
									<option >COMPLETED</option> 
								</select>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								 
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('AssignTrainee');?>
						</div>
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-xs-12">
							 <div id="Information" class="tab-pane fade in active">
								  <div class="row">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<br/>
										<div role="form">	
												<div class="col-lg-12 col-md-12 col-xs-12">
													 <br>
													<button  class="btn btn-primary  waves-effect waves-light m-b-5"  data-toggle="modal" data-target="#myModal"  ><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button>
													<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
													<button   class="btn btn-primary waves-effect waves-light m-b-5" data-toggle="modal" data-target="#uploadModal" ><i class="glyphicon glyphicon-upload"></i> <span class="hidden-xs"><?php echo $this->lang->line('Upload');?></span></button>
												</div>
										</div>
									</div> <br>
									<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('EmployeeCode')?></th>
													<th><?php echo $this->lang->line('TraineeName');?></th>
													<th><?php echo $this->lang->line('Site');?></th>
													<th><?php echo $this->lang->line('Position');?></th>
													<th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>TH001</td>
													<td >Mr.Akaderk Sailim</td> 
													<td >APD</td> 
													<td >Service Center Manager</td> 
													<td> 
														<!-- <button data-toggle="modal" data-target="#myModal"  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button> -->
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr><tr>
													<td>TH002</td>
													<td >Mr.Somchai Sandee</td> 
													<td >APD</td> 
													<td >Courier</td>
													 
													<td>
														<!-- <button data-toggle="modal" data-target="#myModal"  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button> -->
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr><tr>
													<td>TH003</td>
													<td >Mr.Suparak Laopang</td>
													<td >CXM</td> 
													<td >Courier</td>
													 
													<td>
														<!-- <button data-toggle="modal" data-target="#myModal"  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button> -->
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr>
												<tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.code"></td> 
													<td ng-bind="item.name"></td>
													<td ng-bind="item.contact"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
									<!-- /.table-responsive -->
								</div>
								</div>
								</div>
								
							  	<div class="row text-primary  " style="font-size:xx-small;" >
									<div class="col-md-6 col-xs-12 timestampshow text-left">
										Create by Monchai LapphoOlarn (08-08-2018 00:00:00)
									</div>
									<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
										Update by  Monchai LapphoOlarn (08-08-2018 12:30:00)
									</div>
								</div>
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListofClass');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12"> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('ClassCode');?></th>
											<th><?php echo $this->lang->line('ClassName');?></th>
											<th><?php echo $this->lang->line('StartDate');?></th>
											<th><?php echo $this->lang->line('EndDate');?></th>
											<th><?php echo $this->lang->line('Location');?></th>
											<th><?php echo $this->lang->line('Deadline');?></th>
											<th><?php echo $this->lang->line('ClassStatus');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
											<th><?php echo $this->lang->line('SendMail');?></th>
										</tr>
									</thead>
									<tbody>
										<tr>
                                            <td >C001</td> 
                                            <td >Welcome to My Company</td> 
											<td >10 Oct 2018 9:00</td>
											<td >10 Oct 2018 17:00</td>
                                            <td >Head Center</td> 
											<td >01 Oct 2018 17:00</td>
											<td>Open</td>
											<td>Send</td>
                                            <td>
												<button    class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-send"></i> <span class="hidden-xs"><?php echo $this->lang->line('Email');?></span></button>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Assign');?></span></button> 
											</td> 
                                        </tr><tr>
                                            <td >C002</td> 
                                            <td >Welcome to My Company 2</td> 
											<td >10/10/2018 9:00</td>
											<td >10/10/2018 17:00</td>
                                            <td >Head Center</td> 
											<td >01/10/2018 17:00</td>
											<td>Open</td>
											<td>Wait for send</td>
                                            <td>
												<button    class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-send"></i> <span class="hidden-xs"><?php echo $this->lang->line('Email');?></span></button>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Assign');?></span></button> 
											</td> 
                                        </tr>
										<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.code"></td> 
                                            <td ng-bind="item.name"></td>
                                            <td ng-bind="item.contact"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('Trainee');?></h4>
        </div>
        <div class="modal-body">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-xs-12"> 
						<label><span class="text-danger" >*</span><?php echo $this->lang->line('EmployeeCode');?></label>
				</div>
				<div class="col-lg-8 col-md-8 col-xs-12">
					<input type="text" class="form-control" maxlength="60" > <br>
					<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
				</div>
			</div><br>
			<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('EmployeeCode');?></th>
													<th><?php echo $this->lang->line('TraineeName');?></th>
													<th><?php echo $this->lang->line('Site');?></th>
													 
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>TH001</td>
													<td >Mr.Akaderk Sailim</td> 
													<td >APD</td> 
												</tr><tr>
													<td>TH002</td>
													<td >Mr.Somchai Sandee</td> 
													<td >APD</td>										 
												</tr><tr>
													<td>TH003</td>
													<td >Mr.Suparak Laopang</td> 
													<td >CXM</td>
												</tr>
												<tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.code"></td> 
													<td ng-bind="item.name"></td>
													<td ng-bind="item.contact"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>
</div>

<!-- Modal uploadfile-->
 <div class="modal fade" id="uploadModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('AssignTrainee');?></h4>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			<div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Upload');?></label>
					<input type="file"  class="form-control">
				</div> 
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#resultModal"><i class="glyphicon glyphicon-upload"></i> <span class="hidden-xs"><?php echo $this->lang->line('Upload');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>

  <!-- Modal resultupload-->
 <div class="modal fade" id="resultModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('Upload'); echo $this->lang->line('Trainee');?></h4>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			<div class="row"> 
				<div class="col-lg-12 col-md-12 col-xs-12"> 
					<div class="col-lg-6 col-md-6 col-xs-12">
						<h4><?php echo $this->lang->line('Complete');?></h4>
					</div>
					<div class="col-lg-4 col-md-4 col-xs-12">
						<h4>95</h4>
					</div>
					<div class="col-lg-2 col-md-2 col-xs-12"> 
						<h4>Rows</h4>
					</div>
				</div> <br>
				<div class="col-lg-12 col-md-12 col-xs-12">  
						<div class="col-lg-6 col-md-6 col-xs-12">
							<h4><?php echo $this->lang->line('error');?></h4>
						</div>
						<div class="col-lg-4 col-md-4 col-xs-12">
							<h4>5</h4>
						</div>
						<div class="col-lg-2 col-md-2 col-xs-12"> 
							<h4>Rows</h4>
						</div>
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" href="#showdetail">Show Detail</a>
								</h4>
								</div>
								<div id="showdetail" class="panel-collapse collapse">
								<div class="panel-body">
								<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('EmployeeCode')?></th>
													<th><?php echo $this->lang->line('TraineeName');?></th>
													<th><?php echo $this->lang->line('Site');?></th>
													<th><?php echo $this->lang->line('Position');?></th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>001</td>
													<!-- <td >Mr.Akaderk Sailim</td> 
													<td >APD</td> 
													<td >Service Center Manager</td>  -->
												</tr><tr>
													<td>002</td>
													<!-- <td >Mr.Somchai Sandee</td> 
													<td >APD</td> 
													<td >Courier</td> -->
												</tr><tr>
													<td>003</td>
													<!-- <td >Mr.Suparak Laopang</td>
													<td >CXM</td> 
													<td >Courier</td> -->
												</tr> 
											</tbody>
										</table>
									</div>
								</div>
								<!-- <div class="panel-footer">Panel Footer</div> -->
								</div>
							</div>
						</div>
				</div> 
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<!-- <button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-upload"></i> <span class="hidden-xs"><?php echo $this->lang->line('Upload');?></span></button> -->
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Close');?></button>
        </div>
      </div>
      
    </div>
  </div>

<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
	});
	$("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
});
});
</script>
