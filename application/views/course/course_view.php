<script src="<?php echo base_url('asset/courseController.js');?>"></script>
 <div  ng-controller="courseController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator">
		<li class="nav_active"> <?php echo $this->lang->line('ManageCourse');?> --> </li>
		<li class="nav_active"> <?php echo $this->lang->line('Course');?></li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Course');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('CompanyCode');?></label>
								<input class="form-control" ng-model="modelSearch.code" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12"> 
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('CourseType');?></label>
								<select class="form-control">
									<option ></option>
									<option >Mandatory</option>
									<option >Functional</option>
									<option >Soft Skill</option> 
								</select>
								
							</div> 
						</div>
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('Company');?></label>
								<input class="form-control" ng-model="modelSearch.name" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><span class="text-danger" >*</span><?php echo $this->lang->line('Contact');?></label>
								<input class="form-control" ng-model="modelSearch.contact"  maxlength="80">
								<p class="help-block"></p>
							</div> 
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Course');?>
						</div>
						<div class="panel-body">
							
							<div class="col-lg-12 col-md-12 col-xs-12">
							 <ul class="nav nav-tabs">
								<li class="active"><a href="#Information"><?php echo $this->lang->line('Information');?></a></li>
								<!-- <li ng-show="CreateModel.id > 0"><a href="#RequireCourseList"><?php echo $this->lang->line('RequireCourseList');?></a></li> -->
								<li><a href="#RequireCourseList"><?php echo $this->lang->line('RequireCourseList');?></a></li>
								<li><a href="#TrainerList"><?php echo $this->lang->line('TrainerList');?></a></li>
								<li><a href="#MaterialList"><?php echo $this->lang->line('MaterialList');?></a></li>
							  </ul>

							  <div class="tab-content">
								<div id="Information" class="tab-pane fade in active">
								  <div class="row">
									<div class="col-lg-12 col-md-12 col-xs-12">
										<br/>
										<div role="form">
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-6 col-md-6 col-xs-12">&nbsp;</div> 
												<div class="col-lg-6 col-md-6 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('CourseCode');?></label>
													<input class="form-control" ng-model="CreateModel.id"  maxlength="80">
													<p class="CreateModel_coursecode require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
											</div>
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-6 col-md-6 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('CourseName');?></label>
													<input class="form-control" ng-model="CreateModel.course_name" maxlength="80" >
													<p class="CreateModel_coursename require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
												<div class="col-lg-3 col-md-3 col-xs-6">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Abbrivation');?></label>
													<input class="form-control" ng-model="CreateModel.abbrivation" maxlength="80" >
													<p class="CreateModel_abbrivation require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div> 
												<div class="col-lg-3 col-md-3 col-xs-6"> 
													<label><?php echo $this->lang->line('CourseType');?></label>
													<select class="form-control" ng-model="CreateModel.course_type">
														<option ></option>
														<option >Mandatory</option>
														<option >Functional</option>
														<option >Soft Skill</option> 
													</select>
												</div>  
											</div> 
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-12 col-md-12 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Overview');?></label>
													<textarea class="form-control" rows="3" ng-model="CreateModel.overview"  maxlength="80"></textarea>
													<p class="CreateModel_overview require text-danger"><?php echo $this->lang->line('Require');?></p>
													<br/>													
												</div> 
											</div>
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-12 col-md-12 col-xs-12">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Prework');?></label>
													<textarea class="form-control" rows="3" ng-model="CreateModel.pre_work"  maxlength="80"></textarea>
													<p class="CreateModel_prework require text-danger"><?php echo $this->lang->line('Require');?></p>
													<br/>
												</div> 
											</div> 
											<div class="form-group col-lg-12 col-md-12 col-xs-12">
												<div class="col-lg-3 col-md-3 col-xs-4">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('Duration');?> (<?php echo $this->lang->line('Minute');?>)</label>
													<input class="form-control" ng-model="CreateModel.duration" maxlength="80" >
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p> 
												</div> <div class="col-lg-3 col-md-3 col-xs-4">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('CourseExpire');?>(<?php echo $this->lang->line('Month');?>)</label>
													<input class="form-control" ng-model="CreateModel.course_expire" maxlength="80" >
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p> 
												</div> 
												<div class="col-lg-3 col-md-3 col-xs-4">
													<label><span class="text-danger" >*</span><?php echo $this->lang->line('BudgetCost');?></label>
													<input class="form-control" ng-model="CreateModel.budget" maxlength="80" >
													<p class="CreateModel_name require text-danger"><?php echo $this->lang->line('Require');?></p>
												</div>
											</div> 

											<div class="form-group text-right">
												<br/><br/>
												<div class="col-lg-12 col-md-12 col-xs-12">
												<button class="btn btn-primary"  ng-click="onSaveTagClick()" ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
												<button class="btn btn-danger" ng-click="ShowDevice()"><i class="fa fa-times "></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
												</div>
											</div> 
										</div>
									</div>  
								</div>
								</div>
								<div id="RequireCourseList" class="tab-pane fade">
								  <br/>
								  <div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('CourseName');?></th> 
													<th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody>
												<tr> 
													<td><input  class="form-control" type="text" ></td> 
													<td><button  class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button></td>
												</tr>
												
												<!-- <tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.id"></td> 
													<td ng-bind="item.course_name"></td>
													<td ng-bind="item.course_type"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr>  -->
											</tbody>
										</table>
									</div>
								</div>
								<div id="TrainerList" class="tab-pane fade">
								  <br/>
								  <div>
								  
									<button  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button>
											
								  </div>
								  <div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('TraineeCode');?></th>
													<th><?php echo $this->lang->line('TrainerName');?></th> 
													<th><?php echo $this->lang->line('Option');?></th>
													
												</tr>
											</thead>
											<tbody>
												
												<tr> 
													<td>001</td>
													<td>Monchai LapphoOlarn</td> 
													<td>
														<button   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)"  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr><tr> 
													<td>002</td>
													<td>Montri LapphoOlarn</td> 
													<td>
														<button   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)"  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr><tr> 
													<td>003</td>
													<td>Pornchai LapphoOlarn</td> 
													<td>
														<button   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)"  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr>
												
												<tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.code"></td> 
													<td ng-bind="item.name"></td>
													<td ng-bind="item.contact"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
								</div>
								<div id="MaterialList" class="tab-pane fade">
								  <br/>
								  <div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('MaterialName');?></th> 
													<th><?php echo $this->lang->line('Option');?></th>
												</tr>
											</thead>
											<tbody>
												<tr> 
													<td><input  class="form-control" id="file" type="file" ></td> 
													<td><button  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-plus"></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button></td>
												</tr>
												<tr> 
													<td>powerpoint_presentation.ptt</td> 
													<td>
														<button   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-download"></i> <span class="hidden-xs"><?php echo $this->lang->line('Download');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)"  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr><tr> 
													<td>presentation1.pdf</td> 
													<td>
														<button   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-download"></i> <span class="hidden-xs"><?php echo $this->lang->line('Download');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)"  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
												<tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.id"></td> 
													<td ng-bind="item.course_name"></td>
													<td ng-bind="item.course_type"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
								</div>
							  </div>
							  
							  	<div class="row text-primary  " style="font-size:xx-small;" >
									<div class="col-md-6 col-xs-12 timestampshow text-left">
										Create by Monchai LapphoOlarn (08-08-2018 00:00:00)
									</div>
									<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
										Update by  Monchai LapphoOlarn (08-08-2018 12:30:00)
									</div>
								</div>
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfCourse');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button class="btn btn-primary" ng-click="AddNewDevice()" ><i class="fa fa-plus  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Add');?></span></button> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('CourseCode');?></th>
											<th><?php echo $this->lang->line('CourseName');?></th>
											<th><?php echo $this->lang->line('CourseType');?></th>
											<th><?php echo $this->lang->line('Duration') . "(" .  $this->lang->line('Minute') . ")"; ?></th>  
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="item in modelDeviceList">
												<td ng-bind="item.id"></td> 
												<td ng-bind="item.course_name"></td>
												<td ng-bind="item.course_type_name"></td> 
												<td ng-bind="item.duration"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>



 <!-- Modal -->
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('Trainer');?></h4>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			<div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Site');?></label>
					<select class="form-control">
						<option ></option>
						<option >APD</option>
						<option >CXM</option>
						<option >EGW</option> 
					</select>
				</div>
				<!-- <div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><?php echo $this->lang->line('PassStatus');?></label>
					<select class="form-control">
						<option ></option>
						<option >Fail</option>
						<option >Wait to Train</option> 
					</select>
				</div> -->
			</div><div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<br>
					<input type="checkbox" class="check" id="checkAll"> <?php echo $this->lang->line('SelectAll')?>
				</div>
				<div class="col-lg-6 col-md-6 col-xs-12">
					 <br>
					 
				</div>
				
			</div> 
	
			<div class="col-lg-12 col-md-12 col-xs-12">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th></th>
													<th><?php echo $this->lang->line('TraineeName');?></th>
													<th><?php echo $this->lang->line('Position');?></th>
													 
												</tr>
											</thead>
											<tbody>
												<tr>
													<td> <input type="checkbox" class="check"></td>
													<td >Mr.Akaderk Sailim</td> 
													<td >Service Center Manager</td> 
													 
													<td> </td> 
												</tr><tr>
												<td> <input type="checkbox" class="check"></td>
													<td >Mr.Somchai Sandee</td> 
													<td >Courier</td>
													 
													<td></td> 
												</tr><tr>
													<td> <input type="checkbox" class="check"></td>
													<td >Mr.Suparak Laopang</td> 
													<td >Courier</td>
													 
													<td></td> 
												</tr>
												<tr ng-repeat="item in modelDeviceList">
													<td ng-bind="item.code"></td> 
													<td ng-bind="item.name"></td>
													<td ng-bind="item.contact"></td> 
													<td>
														<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
														<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
													</td> 
												</tr> 
											</tbody>
										</table>
									</div>
			<br/><br/>
        </div>
		<div class="col-lg-12 col-md-12 col-xs-12"> 
		<nav aria-label="Page navigation example">
			<ul class="pagination">
				<li class="page-item">
				<a class="page-link" href="#" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
					<span class="sr-only">Previous</span>
				</a>
				</li>
				<li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item"><a class="page-link" href="#">2</a></li>
				<li class="page-item"><a class="page-link" href="#">3</a></li>
				<li class="page-item">
				<a class="page-link" href="#" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
					<span class="sr-only">Next</span>
				</a>
				</li>
			</ul>
			</nav>
		</div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>


<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
	});
	$("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'))});

var uploadField = document.getElementById("file");

uploadField.onchange = function() {
    if(this.files[0].size > 5120){
       alert("File upload should 2-5 MB!");
    };
};
});


</script>