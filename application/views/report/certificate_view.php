<script src="<?php echo base_url('asset/customerController.js');?>"></script>
 <div  ng-controller="customerController" ng-init="onInit()">
 
 <div class="row">
	<ul class="navigator"> 
		<li class="nav_active"> <?php echo $this->lang->line('Certificate');?> 
		</li>
	</ul>
	<!-- /.col-lg-12 -->
</div>

  <div class="row" >
			<div class="col-lg-12">
				<h1 class="page-header"><?php echo $this->lang->line('Certificate');?></h1>
			</div>
                <!-- /.col-lg-12 -->
            </div>
       
			<!-- /List.row types-->
			<div class="row  SearchDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('Search');?>
						</div> 
						<div class="panel-body">
						<div class="form-group col-lg-12 col-md-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('ClassName');?></label>
								<input class="form-control" ng-model="modelSearch.code" maxlength="80" >
								<p class="help-block"></p>
							</div> 
							<div class="col-lg-6 col-md-6 col-xs-12">
								<label><?php echo $this->lang->line('Location');?></label>
								<input class="form-control" ng-model="modelSearch.name" maxlength="80" >
								<p class="help-block"></p>
							</div> 
						</div> 
						<div class="col-lg-12 col-md-12 col-xs-12">
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="resetSearch()"><i class="glyphicon glyphicon-repeat"></i> <span class="hidden-xs"><?php echo $this->lang->line('ResetSearch');?></span></button>
							<button type="button" class="btn btn-primary waves-effect waves-light m-b-5" ng-click="LoadSearch()"><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
							<button type="button" class="btn btn-danger waves-effect waves-light m-b-5" ng-click="ShowDevice()"><i class="fa fa-times"></i> <span class="hidden-xs"><?php echo $this->lang->line('Cancel');?></span></button>
						</div>
						
						</div> 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	   
			  
			<!-- / create room types  -->
			<div class="row addDevice" style="display:none;">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4><?php echo $this->lang->line('ClassName');?> Welcome to My Company</h4>
						</div>
						<div class="panel-body">
							<div class="col-lg-12 col-md-12 col-xs-12"> 
								<h4><?php echo $this->lang->line('CourseName'); ?> : Welcome to My Company</h4> <br>
							</div>
							<div class="col-lg-12 col-md-12 col-xs-12"> 
								<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button> 
								<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 "   ng-click="ShowDevice()" ><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
								<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs"><?php echo $this->lang->line('PrintAll');?></span></button> 
								<br><br>
							</div>
							<div class="col-lg-12 col-md-12 col-xs-12"> 
								<input type="checkbox" class="check" id="checkAll"> <label for=""><?php echo $this->lang->line('SelectAll')?></label>
							</div>
							<div class="col-lg-12 col-md-12 col-xs-12">
								<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('PassStatus');?></th>
											<th><?php echo $this->lang->line('SiteName');?></th>
											<th><?php echo $this->lang->line('TraineeCode');?></th>
											<th><?php echo $this->lang->line('TraineeName');?></th>
											<th><?php echo $this->lang->line('PositionGroup');?></th>
											<th><?php echo $this->lang->line('Position');?></th>
											<th><?php echo $this->lang->line('Score');?></th>
											<th><?php echo $this->lang->line('Remark');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
											
										</tr>
									</thead>
									<tbody>
										<tr >
											<td><input type="checkbox" class="check"></td>
											<td>APD</td>
											<td>M001</td>
											<td>Mr.Akaderk Sailim</td>
											<td>Manager</td>
											<td>Service Center Manager</td> 
											<td>90</td>
											<td>test1</td>
                                            <td>
												<button data-toggle="modal" data-target="#myModal" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">Edit</span></button>
												<button onclick="openFile2()" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs"><?php echo $this->lang->line('Print');?></span></button> 
											</td> 
                                        </tr><tr >
											<td><input type="checkbox" class="check"></td>
											<td>APD</td>
											<td>C001</td>
											<td>Mr.Somchai Sandee</td>
											<td>Ground Ops</td>
											<td>Courier</td> 
											<td>80</td>
											<td>test2</td>
                                            <td>
												<button data-toggle="modal" data-target="#myModal" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">Edit</span></button>
												<button onclick="openFile2()" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs"><?php echo $this->lang->line('Print');?></span></button> 
											</td> 
                                        </tr><tr >
											<td><input type="checkbox" class="check"></td>
											<td>CXM</td>
											<td>C002</td>
											<td>Mr.Suparak Laopang</td>
											<td>Ground Ops</td>
											<td>Courier</td> 
											<td>85</td>
											<td>test3</td>
                                            <td>
												<button data-toggle="modal" data-target="#myModal" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs">Edit</span></button>
												<button onclick="openFile2()"  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-print"></i> <span class="hidden-xs"><?php echo $this->lang->line('Print');?></span></button> 
											</td> 
                                        </tr> 
										<?php /*<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.code"></td> 
                                            <td ng-bind="item.name"></td>
                                            <td ng-bind="item.contact"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> */ ?>
									</tbody>
								</table>
							</div>
							<div class="row text-primary  " style="font-size:xx-small;" >
									<div class="col-md-6 col-xs-12 timestampshow text-left">
										Create by Monchai LapphoOlarn (08-08-2018 00:00:00)
									</div>
									<div class="col-md-6 col-xs-12 timestampshow text-right text-left-xs">
										Update by  Monchai LapphoOlarn (08-08-2018 12:30:00)
									</div>
								</div>
							</div>
							<!-- /.row (nested) -->
						
						</div>
						
						 
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.create room types -->
			
			
			<!-- /List.row types-->
			<div class="row DisplayDevice" >
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<?php echo $this->lang->line('ListOfCertificate');?>
						</div> 
						<div class="panel-body">
						<div class="col-lg-12 col-md-12 col-xs-12"> 
							<button class="btn btn-primary" ng-click="ShowSearch()"><i class="fa fa-search  "></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>  
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
										<tr> 
											<th><?php echo $this->lang->line('ClassCode');?></th>
											<th><?php echo $this->lang->line('ClassName');?></th>
											<th><?php echo $this->lang->line('CourseCode');?></th>
											<th><?php echo $this->lang->line('StartDate');?></th>
											<th><?php echo $this->lang->line('EndDate');?></th>
											<th><?php echo $this->lang->line('Location');?></th>
											<th><?php echo $this->lang->line('Deadline');?></th>
											<th><?php echo $this->lang->line('ClassStatus');?></th>
											<th><?php echo $this->lang->line('Option');?></th>
										</tr>
									</thead>
									<tbody>
										<tr>
                                            <td >C001</td> 
											<td >Welcome to My Company</td> 
											<td>001</td>
											<td >10 Oct 2018 09:00</td>
											<td >10 Oct 2018 12:30</td>
                                            <td >Head Center</td> 
											<td >01 Oct 2018 17:00</td>
											<td>OPEN</td>
                                            <td> 
												<button ng-click="AddNewDevice()"  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-tasks"></i> <span class="hidden-xs"><?php echo $this->lang->line('Manage');?></span></button>
											</td> 
										</tr><tr>
                                            <td >C001</td> 
											<td >Welcome to My Company</td> 
											<td>002</td>
											<td >10 Oct 2018 13:00</td>
											<td >10 Oct 2018 17:00</td>
                                            <td >Head Center</td> 
											<td >01 Oct 2018 17:00</td>
											<td>OPEN</td>
                                            <td> 
												<button ng-click="AddNewDevice()"  class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-tasks"></i> <span class="hidden-xs"><?php echo $this->lang->line('Manage');?></span></button>
											</td> 
                                        </tr><tr>
                                            <td >C002</td> 
                                            <td >Welcome to My Company 2</td>
											<td>003</td>
											<td >10 Oct 2018 09:00</td>
											<td >10 Oct 2018 12:30</td>
                                            <td >Head Center</td> 
											<td >01 Oct 2018 17:00</td>
											<td>OPEN</td>
                                            <td> 
												<button  ng-click="AddNewDevice()" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-tasks"></i> <span class="hidden-xs"><?php echo $this->lang->line('Manage');?></span></button>
											</td> 
                                        </tr><tr>
                                            <td >C002</td> 
                                            <td >Welcome to My Company 2</td>
											<td>004</td>
											<td >10 Oct 2018 13:00</td>
											<td >10 Oct 2018 17:00</td>
                                            <td >Head Center</td> 
											<td >01 Oct 2018 17:00</td>
											<td>OPEN</td>
                                            <td> 
												<button  ng-click="AddNewDevice()" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-tasks"></i> <span class="hidden-xs"><?php echo $this->lang->line('Manage');?></span></button>
											</td> 
                                        </tr>
										<tr ng-repeat="item in modelDeviceList">
                                            <td ng-bind="item.code"></td> 
                                            <td ng-bind="item.name"></td>
                                            <td ng-bind="item.contact"></td> 
                                            <td>
												<button ng-click="onEditTagClick(item )" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Edit');?></span></button>
												<button my-confirm-click="onDeleteTagClick(item)" my-confirm-click-message="<?php echo $this->lang->line('DoYouWantToDelete');?>" class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Delete');?></span></button>
											</td> 
                                        </tr> 
									</tbody>
								</table>
							</div>
							<!-- /.table-responsive -->
						</div>
						
						  <!-- ทำหน้า -->
                            <div class="row tblResult small"  >
                                <div class="col-md-7 col-sm-7 col-xs-12 ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('Total');?> {{totalRecords}} <?php echo $this->lang->line('Records');?>
                                    </label>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <?php echo $this->lang->line('ResultsPerPage');?>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-12 ">
                                        <ui-select ng-model="TempPageSize.selected" ng-change="loadByPageSize()" theme="selectize">
                                            <ui-select-match>{{$select.selected.Value}}</ui-select-match>
                                            <ui-select-choices repeat="pSize in listPageSize | filter: $select.search">
                                                <span ng-bind-html="pSize.Text | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-xs-12  ">
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        <span ng-click="getBackPage()" class="set-pointer"><i class="fa fa-chevron-left"></i>  <span class="hidden-xs"><?php echo $this->lang->line('Previous');?></span></span>
                                    </label>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <ui-select ng-model="TempPageIndex.selected" ng-change="searchByPage()" theme="selectize">
                                            <ui-select-match>{{$select.selected.PageIndex}}</ui-select-match>
                                            <ui-select-choices repeat="pIndex in listPageIndex | filter: $select.search">
                                                <span ng-bind-html="pIndex.PageIndex | highlight: $select.search"></span>
                                            </ui-select-choices>
                                        </ui-select>
                                    </div>
                                    <label class="col-md-4 col-sm-4 col-xs-12">
                                        / {{ totalPage }}  <span ng-click="getNextPage()" class="set-pointer"><?php echo $this->lang->line('Next');?><i class="fa fa-chevron-right set-pointer"></i></span>
                                    </label>
                                </div>
                            </div>
                            <!-- ทำหน้า -->
						
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /List.row types-->
	</div>
</div>

 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $this->lang->line('Certificate');?></h4>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
		  <div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Score');?></label>
					<input type="text"  class="form-control">
				</div> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('PassStatus');?></label>
					<select class="form-control">
						<option>Pass</option>
						<option>Fail</option>
					</select>
				</div> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Remark');?></label>
					<input type="text"  class="form-control">
				</div> 
			</div> 
			<div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('AttendanceFile');?></label>
					<input type="file"  class="form-control">
				</div> 
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-upload"></i> <span class="hidden-xs"><?php echo $this->lang->line('Upload');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>
  
 <script  >
	function openFile(){
		path = "<?php echo base_url('materia/Registration form.jpg');?>";
		window.open(path, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
	}
	
	function openFile2(){
		path = "<?php echo base_url('materia/certificate.png');?>";
		window.open(path, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=600,height=800");
	}
	$(document).ready(function(){
		$("#checkAll").click(function () {
		$(".check").prop('checked', $(this).prop('checked'));
	});
	});
</script>
