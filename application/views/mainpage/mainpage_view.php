<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="<?php echo base_url('/theme/image/favicon.png');?>" />
    <title><?php echo $this->lang->line('OTAA');?></title>

     <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('asset/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet">

    <!-- Custom fonts for this template
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   -->
     <!-- Custom Fonts -->
    <link href="<?php echo base_url('asset/font-awesome/css/font-awesome.min.css');?>"  rel="stylesheet" type="text/css">
	
    <link href="<?php echo base_url('asset/devicons/css/devicons.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('asset/simple-line-icons/css/simple-line-icons.css');?>" rel="stylesheet"> 
	

    <!-- Custom styles for this template -->
    <link  href="<?php echo base_url('asset/css/training.min.css');?>"  rel="stylesheet">
	
	 <style>
		.demo-topbar {
				  height: 40px;
				  line-height: 40px;
				  padding-left: 1em;
				  background: #eee;
				  border-bottom: 1px solid #ddd;
				  font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif !important;
				  font-size: 14px !important;
				  color: #000 !important; }
				  .demo-topbar .codepen-button {
					float: right;
					margin-top: 7px;
					margin-right: 7px; }

		.codepen-button {
				  -webkit-appearance: none;
				  -moz-appearance: none;
				  appearance: none;
				  height: 26px;
				  line-height: 26px;
				  padding: 0 6px;
				  border: 1px solid #ddd;
				  background: #fff;
				  border-radius: 4px;
				  font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif !important;
				  font-size: 11px !important;
				  color: #000 !important;
				  cursor: pointer; }
				  .codepen-button:after {
					content: '\02197';
					vertical-align: middle;
					margin: -50% 0 -50% 2px; }
	 </style>
  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
        <span class="d-block d-lg-none">Start Bootstrap</span>
        <span class="d-none d-lg-block">
          <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="<?php echo base_url('theme/image/profile.jpg');?>" alt="">
        </span>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">Calendar Class</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#experience">Check Your Course</a>
          </li> 
		  <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="<?php echo base_url('/Login');?>">Manage OTTA</a>
          </li> 
        </ul>
      </div>
    </nav>

    <div class="container-fluid p-0">

      <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
        <div class="my-auto">
          <h1 class="mb-0">DHL
            <span class="text-primary"><?php echo $this->lang->line('OTAA');?></span>
          </h1>
         <!-- <div class="subheading mb-5">3542 Berry Street · Cheyenne Wells, CO 80810 · (317) 585-8468 ·
            <a href="mailto:name@email.com">name@email.com</a>
          </div>-->
		  <div class="row" >
			  <div style="width:700px;float:left;" id='calendar'></div>
			  
			  <div style=" float:left; margin-top: 50px; margin-left: 10px;display:none;" id="infoclass"  >
				  <h6>Class room information</h6>
				  <p >
						Class Name: <span class="text-primary" id="etitle"> </span> <br>
						Start: <span class="text-primary" id="startTime" name="startTime"></span><br>
						End: <span class="text-primary" id="endTime"></span><br><br> 
						Course 1: <span class="text-primary" id="course1"></span><br><br> 
						Start Course : <span class="text-primary" id="startTimeCourse1"></span><br><br>
						End Course : <span class="text-primary" id="endTimeCourse1"></span><br><br>
						trainner : <span class="text-primary" id="trainner1"></span><br><br> 
						
						<!-- <p><strong><a id="eventLink" href="" ata-toggle="modal">Read More</a></strong></p> -->
						<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#memregis_Modal"    > <span class="hidden-xs"><?php echo('Show Trainee >>');?></span></button> <br><br>
						<button class="btn btn-success waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"    > <span class="hidden-xs"><?php echo('Register');?></span></button> 
				  </p><br/>

					  <p >
						Course 2: <span class="text-primary" id="course2"></span><br><br> 
						Start Course : <span class="text-primary" id="startTimeCourse2"></span><br><br>
						End Course : <span class="text-primary" id="endTimeCourse2"></span><br><br>
						trainner : <span class="text-primary" id="trainner2"></span><br><br> 
						<!-- <p><strong><a id="eventLink" href="" ata-toggle="modal" target="#memregis_Modal">Read More</a></strong></p> -->
						<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#memregis_Modal"    > <span class="hidden-xs"><?php echo('Show Trainee >>');?></span></button> <br><br>
						<button class="btn btn-success waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"    > <span class="hidden-xs"><?php echo('Register');?></span></button> 
				  </p><br/>
			  </div>
		  </div>
          <p class="mb-5"> 
			<br/>
			HR Learning & Development supports the development of group and organizational capacity for continuous learning through our plan.
		  </p> 
        </div>
      </section>

      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="experience">
        <div class="my-auto">
          <h2 class="mb-5">Check Your Course</h2>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-xs-12">
					
						<label>Please input your employee number</label>
						<input class="form-control" maxlength="80" > <br>
						
					<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ><i class="fa fa-search"></i> <span class="hidden-xs"><?php echo $this->lang->line('Search');?></span></button>
				</div> 
			</div><br/>
			<div class="col-lg-12 col-md-12 col-xs-12">
				<h5><?php echo $this->lang->line('Site');?> : APD</h5> <br>
				<h5><label for="">Name : Mr.Akaderk Sailim </label></h5>
			</div> <br>
			<div class="row">
			<div class="col-lg-12 col-md-12 col-xs-12">
				<div class="table-responsive">
					Your next classroom.
					<table class="table table-striped">
						<thead>
							<tr> 
								<th><?php echo $this->lang->line('ClassCode');?></th>
								<th style="border-right:0px;"><?php echo $this->lang->line('ClassName');?></th>
								<th><?php echo $this->lang->line('StartDate');?></th>
								<th><?php echo $this->lang->line('EndDate');?></th>
								<th><?php echo $this->lang->line('Location');?></th>
								<th><?php echo $this->lang->line('Deadline');?></th>
								<th><?php echo $this->lang->line('ClassStatus');?></th>
								<th><?php echo $this->lang->line('Option');?></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td >C001</td> 
								<td style="border-right:0px;,border-left:0px;">Welcome to My Company</td> 
								<td >10 Oct 2018 09:00</td>
								<td >10 Oct 2018 12:00</td>
								<td >Head Center</td> 
								<td >01 Oct 2018 17:00</td>
								<td>Open</td>
								<td>
									<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"    ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Confirm');?></span></button>
									<button  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"  ><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Reject');?></span></button>
									<button  class="btn btn-info waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#classdetailModal"  > <span class="hidden-xs"><?php echo $this->lang->line('ViewDatail');?></span></button>
								</td> 
							</tr>
							<tr>
								<td >C002</td> 
								<td >Welcome to My Company 2</td> 
								<td >10 Oct 2018 13:00</td>
								<td >10 Oct 2018 18:00</td>
								<td >Head Center</td> 
								<td >01 Oct 2018 17:00</td>
								<td>Open</td>
								<td>
									<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"    ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Confirm');?></span></button>
									<button  class="btn btn-danger waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#myModal"  ><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Reject');?></span></button>
									<button  class="btn btn-info waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#classdetailModal"  > <span class="hidden-xs"><?php echo $this->lang->line('ViewDatail');?></span></button>
								</td> 
							</tr>
							<tr>
								<td >C003</td> 
								<td style="border-right:0px">Welcome to My Company</td>
								<td >10 Oct 2018 09:00</td>
								<td >10 Oct 2018 12:00</td>
								<td >Head Center</td> 
								<td >01 Oct 2018 17:00</td>
								<td>Open</td>
								<td>
									<button  data-toggle="modal" data-target="#myModal"   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Confirm');?></span></button>
									<button   data-toggle="modal" data-target="#myModal"   class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Reject');?></span></button>
									<button  class="btn btn-info waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#classdetailModal"  > <span class="hidden-xs"><?php echo $this->lang->line('ViewDatail');?></span></button>
								</td> 
							</tr> 
							<tr>
								<td >C004</td> 
								<td >Welcome to My Company 2</td>
								<td >10 Oct 2018 13:00</td>
								<td >10 Oct 2018 18:00</td>
								<td >Head Center</td> 
								<td >01 Oct 2018 17:00</td>
								<td>Open</td>
								<td>
									<button  data-toggle="modal" data-target="#myModal"   class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="glyphicon glyphicon-edit"></i> <span class="hidden-xs"><?php echo $this->lang->line('Confirm');?></span></button>
									<button   data-toggle="modal" data-target="#myModal"   class="btn btn-danger waves-effect waves-light btn-sm m-b-5"><i class="glyphicon glyphicon-trash"></i> <span class="hidden-xs"><?php echo $this->lang->line('Reject');?></span></button>
									<button  class="btn btn-info waves-effect waves-light btn-sm m-b-5"  data-toggle="modal" data-target="#classdetailModal"  > <span class="hidden-xs"><?php echo $this->lang->line('ViewDatail');?></span></button>
								</td> 
							</tr> 
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<div class="form-group col-lg-12 col-md-12 col-xs-12">
				<div class="table-responsive">
					Your historical training record
					<table class="table table-striped">
						<thead>
							<tr> 
								<th><?php echo $this->lang->line('CertificateDate');?></th>
								<th><?php echo $this->lang->line('CourseName');?></th> 
								<th><?php echo $this->lang->line('ClassName');?></th> 
								<th><?php echo $this->lang->line('ExpireDate');?></th>
								<th><?php echo $this->lang->line('Option');?></th>
							</tr>
						</thead>
						<tbody>
							<tr >
								<td>13 Oct 2018</td>
								<td>CIS - Welcome to My Company</td>
								<td>Welcome to My Company</td>
								<td></td>
								<!-- <td><a data-toggle="modal" href="#regisFollw"><label><?php echo $this->lang->line('Evaluation');?></label></a></td> -->
								<td><button data-toggle="modal" data-target="#regisFollw" class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ><?php echo $this->lang->line('Evaluation');?></span></button></td>
							</tr><tr >
								<td>12 Oct 2018</td>
								<td>CIS - Welcome to My Country</td>	
								<td>Welcome to My Company</td>					
								<td>12 Oct 2020</td>
								<td><button data-toggle="modal" data-target="#regisFollw" class="btn btn-primary waves-effect waves-light btn-sm m-b-5" ><?php echo $this->lang->line('Evaluation');?></span></button></td>
							</tr><tr >
								<td>10 Oct 2018</td>
								<td>First Step</td>
								<td>Welcome to My Company</td>
								<td></td>
								<td><button data-toggle="modal" data-target="#regisFollw" class="btn btn-primary waves-effect waves-light btn-sm m-b-5" disabled><?php echo $this->lang->line('Evaluation');?></span></button></td></td>
							</tr> 
						</tbody>
					</table>
				</div>
			</div> 
			</div>

        </div>

      </section>
 

    </div>

   	   <!-- jQuery -->
    <script src="<?php echo base_url('asset/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core JavaScript --> 
	<script src="<?php echo base_url('asset/bootstrap/js/bootstrap.bundle.js');?>"></script>
	
	 <!-- Plugin JavaScript -->
    <script src="<?php echo base_url('asset/jquery-easing/jquery.easing.min.js');?>"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url('asset/training.min.js');?>"></script>
	
	<!-- Morris Charts JavaScript -->
	<script src="<?php echo base_url('theme/raphael/raphael.min.js');?>"></script>
	<script src="<?php echo base_url('theme/morrisjs/morris.min.js');?>"></script> 
	
	<!-- asset -->  
	<link href="<?php echo base_url('asset/fullcalendar/fullcalendar.min.css');?>" rel='stylesheet' />
	<link href="<?php echo base_url('asset/fullcalendar/fullcalendar.print.css');?>" rel='stylesheet' media='print' />
	<script src="<?php echo base_url('asset/fullcalendar/moment.min.js');?>"></script>
	<script src="<?php echo base_url('asset/fullcalendar/fullcalendar.js');?>"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'></script>
	
	
	<!-- Bootstrap Core JavaScript --> 
	<script src="<?php echo base_url('theme/bootstrap/js/bootstrap.min.js');?>"></script>
	<script src="<?php echo base_url('asset/fullcalendar/codepen.js');?>"></script>

	<div id="eventContent" title="Event Details" style="display:none;">
	
	</div>
	
	 
  

	<div class='demo-topbar'> 
		Devdee (Thailand) Co.,Ltd. © 2016. All Rights Reserved.
	</div>
	
	<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
			 <span class="modal-title"><?php echo $this->lang->line('Confirm');?></span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			 <div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-6"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('UserName');?></label>
					 <input class="form-control" />  <br>
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Password');?></label>
					 <input class="form-control" /> 
				</div>
				<div class="col-lg-6 col-md-6 col-xs-6">  
					 <label><input type="radio" name="confirmcheck" checked /> confirm</label><br/>
					 <label><input type="radio" name="confirmcheck"  /> reject</label>
				</div>
				<div class="col-lg-6 col-md-6 col-xs-6">  
						<br><br>
					 	<a data-toggle="modal" href="#passwordModal"><label><?php echo $this->lang->line('ForgetPassword');?></label></a>
				</div>
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>

	<!-- Large modal -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" id="memregis_Modal"role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
		<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr> 
													<th><?php echo $this->lang->line('Site');?></th>
													<th><?php echo $this->lang->line('TraineeName');?></th>
													<th><?php echo $this->lang->line('Position');?></th>
													 
												</tr>
											</thead>
											<tbody>
												<tr>
													<td >APD</td> 
													<td >Mr.Akaderk Sailim</td> 
													<td >Service Center Manager</td> 
													 <td></td>
												</tr><tr>
													<td >APD</td> 
													<td >Mr.Somchai Sandee</td> 
													<td >Courier</td>
													 
													<td></td> 
												</tr><tr>
													<td >CXM</td> 
													<td >Mr.Suparak Laopang</td> 
													<td >Courier</td>
													 
													<td></td> 
											</tbody>
										</table>
									</div>
    </div>
  </div>
</div>

<!-- Modal Forgetpassword-->
<div class="modal fade" id="passwordModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header text-primary">
			 <span class="modal-title"><?php echo $this->lang->line('ForgetPassword');?></span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			 <div class="row"> 
				<div class="col-lg-12 col-md-12 col-xs-12"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Email');?></label>
					 <input class="form-control" />  <br>
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('UserName');?></label>
					<input class="form-control" />  <br>
				</div>
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  > <span class="hidden-xs">Submit</span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>

	<!-- Modal classdetail-->
<div class="modal fade bd-example-modal-lg" id="classdetailModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header text-primary">
			 <span class="modal-title">Welcome to My Company</span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
					<div class="row"> 
						<div class="col-md-5"> 
							<b><?php echo $this->lang->line('StartDate');?> : </b> 10 Oct 2018 09:00
						</div>
						<div class="col-md-4"> 
							<b><?php echo $this->lang->line('EndDate');?> : </b>  10 Oct 2018 18:00
						</div>
					</div><br>
					<div class="row"> 
						<div class="col-md-3"> 
							<b><?php echo $this->lang->line('Location');?> : </b>  Head Center
						</div>
						<div class="col-md-3"> 
							<b><?php echo $this->lang->line('Rooms');?> :</b> M001
						</div>
						<div class="col-md-6"> 
							<b><?php echo $this->lang->line('Deadline');?> : </b> 1 Oct 2018 17:00
						</div>
					</div><br>
					<div class="row">
						<b><div class="col-md-12"><?php echo $this->lang->line('OutlineIntroduce');?></div></b><br>
						<div class="col-md-12"><p>About Company</p></div>
					</div><br>
					<div class="row">
						<b><div class="col-md-12"><?php echo $this->lang->line('MaterialAcquired');?></div></b><br>
						<div class="col-md-12"><p>About Company</p></div>
					</div><br>
					<div class="row">
						<b><div class="col-md-12"><?php echo $this->lang->line('Prework');?></div></b><br>
						<div class="col-md-12"><p>About Company</p></div>
					</div><br>
					<div class="row">
						<b><div class="col-md-12"><?php echo $this->lang->line('Trainer');?></div></b><br>
						<div class="col-md-12"><p>Monchai LapphoOlarn</p></div>
					</div><br>
					<div class="row"> 
						<div class="col-md-4"> 
							<b><?php echo $this->lang->line('TransportationPolicy');?>  </b> <br> BTS 
						</div>
						<div class="col-md-4"> 
							<b><?php echo $this->lang->line('ParkingPolicy');?> </b> <br> Yes
						</div>
						<div class="col-md-4"> 
							<b><?php echo $this->lang->line('DressingPolicy');?>  </b> <br>Formal
						</div>
					</div><br>
         <div>	
				
			</div> 
					
			<br/><br/>
        </div>
        <div class="modal-footer">
					<button type="button" class="btn btn-primary waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Close');?></button>
        </div>
      </div>
      
    </div>
  </div>
	
	<!-- Modal Regis Follow Up -->
		<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
			 		<span class="modal-title"><?php echo $this->lang->line('Confirm');?></span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			 <div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-6"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('UserName');?></label>
					 <input class="form-control" />  <br>
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Password');?></label>
					 <input class="form-control" /> 
				</div>
				<div class="col-lg-6 col-md-6 col-xs-6">  
					 <label><input type="radio" name="confirmcheck" checked /> confirm</label><br/>
					 <label><input type="radio" name="confirmcheck"  /> reject</label>
				</div>
				<div class="col-lg-6 col-md-6 col-xs-6">  
						<br><br>
					 	<a data-toggle="modal" href="#passwordModal"><label><?php echo $this->lang->line('ForgetPassword');?></label></a>
				</div>
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
			<button class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><i class="fa fa-save"></i> <span class="hidden-xs"><?php echo $this->lang->line('Save');?></span></button>
			<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><i class="fa fa-times"></i> <?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>

	<!-- Modal Regis FollwUp -->
	<div class="modal fade" id="regisFollw" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-primary">
			 <span class="modal-title"><?php echo $this->lang->line('Confirm');?></span>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body">
          <p>&nbsp;</p>
			 <div class="row"> 
				<div class="col-lg-6 col-md-6 col-xs-6"> 
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('UserName');?></label>
					 <input class="form-control" />  <br>
					<label><span class="text-danger" >*</span><?php echo $this->lang->line('Password');?></label>
					 <input class="form-control" /> 
				</div>
				<div class="col-lg-12 col-md-12 col-xs-12">  
						<br><br>
					 	<a data-toggle="modal" href="#passwordModal"><label><?php echo $this->lang->line('ForgetPassword');?></label></a>
				</div>
			</div> 
			<br/><br/>
        </div>
        <div class="modal-footer">
					<button  data-toggle="modal" href="#evaluateModal" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><span class="hidden-xs"><?php echo $this->lang->line('Confirm');?></span></button>
					<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><?php echo $this->lang->line('Cancel');?></button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Modal Evaluate Follw Up -->
<div class="modal fade" id="evaluateModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
				<div class="modal-header text-primary">
						<span class="modal-title"><?php echo $this->lang->line('Evaluation');?></span>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					 <div class="row"> 
						<div class="col-lg-6 col-md-6 col-xs-12"> 
							<br><br>
							<label><?php echo $this->lang->line('TraineeName');?></label>
							<input type="text" value="Mr.Akaderk Sailim" readonly="readonly"  class="form-control"> 
						</div> 
						<div class="col-lg-6 col-md-6 col-xs-12"> 
							<br><br>
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('Status');?></label>
							<select  class="form-control">
								<option>Wait</option>
								<option>Pass</option>
								<option>Fail</option>
							</select>
							<br><br>
						</div>
						<div class="col-lg-12 col-md-12 col-xs-12"> 
							<label><span class="text-danger" >*</span><?php echo $this->lang->line('Comment');?></label>
							<textarea class="form-control">
							</textarea>
							<br><br><br>
						</div> 
					</div>
				</div>
				<div class="modal-footer">
					<button  data-toggle="modal" href="#" class="btn btn-primary waves-effect waves-light btn-sm m-b-5"  ><span class="hidden-xs"><?php echo $this->lang->line('Confirm');?></span></button>
					<button type="button" class="btn btn-danger waves-effect waves-light btn-sm m-b-5 " data-dismiss="modal"><?php echo $this->lang->line('Cancel');?></button>
        </div>
			</div>
    </div>
  </div>


</body>

</html>


<script>


  $(document).ready(function() {

    $('#calendar').fullCalendar({
      defaultDate: new Date(),// '2018-06-14',
	 // disableDragging: true,
	  eventRender: function(event, element) {
	    element.attr('href', 'javascript:void(0);');
		element.click(function() {
			$("#infoclass").show();
			$("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
			$("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
			$("#etitle").html(event.title);
			$("#course1").html(event.course1);
			$("#course2").html(event.course2);
			$("#startTimeCourse1").html(moment(event.startcourse1).format('MMM Do h:mm A'));
			$("#endTimeCourse1").html(moment(event.endcourse1).format('MMM Do h:mm A'));
			$("#startTimeCourse2").html(moment(event.startcourse2).format('MMM Do h:mm A'));
			$("#endTimeCourse2").html(moment(event.endcourse2).format('MMM Do h:mm A'));
			$("#trainner1").html(event.trainner1);
			$("#trainner2").html(event.trainner2);
			//$("#eventLink").attr('href', event.url);
			//$("#myModal").modal("show");  //.dialog({ modal: true, title: event.title, width:350});
		});
      },
      //editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'Welcome to My Company',
          start: '2018-06-12T10:30:00',
          end: '2018-06-12T18:30:00',
		  course1: 'CIS - Welcome to My Company',
			startcourse1: '2018-06-12T10:30:00',
			endcourse1: '2018-06-12T12:30:00',
		  trainner1: 'Mr.Sakkarin ',
			course2: 'CIS - Welcome to My Company 2',
			startcourse2: '2018-06-12T13:30:00',
			endcourse2: '2018-06-12T18:30:00',
		  trainner2: 'Mr.Suchart '
        },
        {
					title: 'Welcome to My Company',
          start: '2018-06-14T09:30:00',
          end: '2018-06-14T18:00:00',
		  course1: 'CIS - Welcome to My Company',
			startcourse1: '2018-06-14T09:30:00',
			endcourse1: '2018-06-14T12:00:00',
		  trainner1: 'Mr.Sakkarin ',
			course2: 'CIS - Welcome to My Company 2',
			startcourse2: '2018-06-14T13:00:00',
			endcourse2: '2018-06-14T18:00:00',
		  trainner2: 'Mr.Suchart '
        },
        {
          title: 'All Welcome',
          start: '2018-06-15T10:30:00',
          end: '2018-06-15T18:30:00',
		  course1: 'First Step',
			startcourse1: '2018-06-15T10:30:00',
			endcourse1: '2018-06-15T12:30:00',
		  trainner1: 'Mr.Sakkarin ',
			course2: 'CIS - Welcome to My Company',
			startcourse2: '2018-06-15T13:30:00',
			endcourse2: '2018-06-15T18:30:00',
		  trainner2: 'Mr.Suchart '
        } ,
				{
          title: 'All Welcome',
          start: '2018-09-04T09:30:00',
          end: '2018-09-04T18:00:00',
		  course1: 'First Step',
			startcourse1: '2018-09-04T09:30:00',
			endcourse1: '2018-09-04T12:00:00',
		  trainner1: 'Mr.Sakkarin ',
			course2: 'CIS - Welcome to My Company',
			startcourse2: '2018-09-04T13:00:00',
			endcourse2: '2018-09-04T18:00:00',
		  trainner2: 'Mr.Suchart '
        } 
      ]
    }); 

  });
  
  

</script>
