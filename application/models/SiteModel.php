<?php
  
class SiteModel extends CI_Model {
	
    private $tbl_name = 'site';
	private $id = 'id';
 
    public function __construct() {
        parent::__construct();
    }
	
	public function getSiteById($id){
		$this->db->where($this->id, $id);
		return $this->db->get($this->tbl_name);
	}
	
	public function insert($modelData){
		 
	 	$this->db->insert($this->tbl_name, $modelData); 
		return $this->db->insert_id(); 
    }
     
    public function update($id, $modelData){
        $this->db->where($this->id, $id);
        return $this->db->update($this->tbl_name, $modelData);
    }
	
	public function getSiteAllList(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSiteModel(){
        //return $this->db->count_all($this->tbl_name);
        
        //$this->db->select('id','name','contact','address1','address2','address3','tel','email','taxid','website');
		//$this->db->where('Site_delete_flag', 0);
        $query =  $this->db->get($this->tbl_name);
		
		return $query->result_array();
    }
	
	public function getSearchQuery($sql, $dataModel){
		
		//เดี๋ยว โอ ต้องเปลี่ยนค่า ตรงนี้ให้ สอดคล้องกับชื่อใน ดาต้าเบส
		
		/*if(isset($dataModel['code']) && $dataModel['code'] != ""){
		 	$sql .= " and code like '%".$this->db->escape_str( $dataModel['code'])."%' ";
		}*/
		
		if(isset($dataModel['site_name']) && $dataModel['site_name'] != ""){
		 	$sql .= " and site_name like '%".$this->db->escape_str( $dataModel['site_name'])."%' ";
		}
		
		if(isset($dataModel['description']) && $dataModel['description'] != ""){
		 	$sql .= " and description like '%".$this->db->escape_str( $dataModel['description'])."%' ";
		}
		
		return $sql;
	}
	
	public function getTotal($dataModel ){
		
		$sql = "SELECT * FROM ". $this->tbl_name  ." WHERE delete_flag = 0  ";
				
		$sql =  $this->getSearchQuery($sql, $dataModel);
		
		$query = $this->db->query($sql);		 
		
		return  $query->num_rows() ;
	}
	
	public function getSiteList($dataModel, $limit = 10, $offset = 0, $order = '', $direction = 'asc'){
		
		$sql = "SELECT * FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		
		$sql =  $this->getSearchQuery($sql, $dataModel);		
		
		if($order != ""){
			$sql .= " ORDER BY ".$order." ".$direction;
		}else{
			$sql .= " ORDER BY ".$this->id." ".$direction;
		}
		
		$query = $this->db->query($sql);
		//$query = $this->db->query($sql, array( "%".$dataModel['Site_name']."%"));// $dataModel);
		
		return  $query->result_array();
	}		
	
	public function deleteSitename($id){
		$result = false;
		try{
			$query = $this->getSiteById($id);
			$modelData;			
			foreach ($query->result() as $row)
			{
			   		
				$modelData = array( 
					'update_date' => date("Y-m-d H:i:s"),
					'update_user' => $this->session->userdata('user_name'),
					'delete_flag' => 1 //$row->Site_delete_flag 
				); 
			}
			
			$this->db->where($this->id, $id);
        	return $this->db->update($this->tbl_name, $modelData);
			//return $this->update($id, $modelData);
			//$sql = "Delete FROM ". $this->tbl_name; 
			//return  $this->db->query($sql);
			
		}catch(Exception $ex){
			return $result;
		}
    }
	
	public function getSiteComboList(){
		
		$sql = "SELECT id, 	name FROM ". $this->tbl_name . " WHERE delete_flag = 0  ";
		$query = $this->db->query($sql);
		return  $query->result_array();
	}
	
}
?>