myApp.controller('systemConfigController', ['$scope', '$filter',  'baseService', 'systemApiService', function ($scope, $filter,  baseService, systemApiService ) {
    $scope.CreateModel = { id:0, name : "", contact: "", address1 : "", address2: "", address3:"", tel:"", email:"", taxid: "", website: ""};

	 
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('RC');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearchDevice = function () {
        $scope.ShowDevice();
       
    } 

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () { 
		 
		systemApiService.getCompany(null, function (result) {  
			if(result.status === true){ 
				$scope.CreateModel = result.message[0]; 
			}else{
				baseService.showMessage(result.message);
			}
		})

        /*$scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
        });


        loadResultTo(masterApiService.listCustomer, $scope, "CustomerDataList");
        loadResultTo(masterApiService.listComboBoxRefElectrode, $scope, "RefElectrodes", null);
        loadResultTo(masterApiService.listComboBoxAnodeType, $scope, "AnodeTypes", null);

        loadResultTo(masterApiService.listAllRegionPipeRoute, $scope, "RegionList", { dt: '' });
        loadResultTo(masterApiService.listAllComboBoxPipeRoute, $scope, "RouteList", { dt: '' });
        loadResultTo(masterApiService.listAllComboBoxRectifier, $scope, "RectList", { dt: '' });
 

        baseService.setSortIcon();
        $scope.reload();*/
    }

    $scope.resetSearch = function () {
         
    }

     

    $scope.reload = function () { 
        
    }
	
	 $scope.onSaveTagClick = function () {
    
		console.log($scope.CreateModel);
        /*if ($scope.CriteriaModel  !== undefined
            && $scope.CriteriaModel.CRITERIA_DESC !== ""
        ) {*/
            //console.log($scope.CriteriaModel);
            systemApiService.saveCompany($scope.CreateModel, function (result) { 
				console.log(result);
                /*$scope.reload();
                $scope.getCurrentData();
                baseService.showMessage("Save success");*/
            })

        /*} else {
            baseService.showMessage("Please input description");
        }*/
    }


}]); 