myApp.controller('courseController', ['$scope', '$filter',  'baseService', 'courseApiService','courseTypeApiService' , 
function ($scope, $filter,  baseService, courseApiService , courseTypeApiService) {
     
	$scope.modelDeviceList = [];
	$scope.CreateModel = {};
	$scope.modelSearch = {}; 
	$scope.listcourseType = [];
	$scope.TempCourseTypeIndex = {};
	
    //page system 
    $scope.listPageSize = baseService.getListPageSize();
    $scope.TempPageSize = {};
    $scope.TempPageIndex = {};
    $scope.PageSize = baseService.setPageSize(20);;
    $scope.totalPage = 1; //init;
    $scope.totalRecords = 0;
    $scope.PageIndex = 1;
    $scope.SortColumn = baseService.setSortColumn('id');
    $scope.SortOrder = baseService.setSortOrder('asc');

    $scope.isView = false;

    $scope.sort = function (e) {
        baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
        $scope.SortOrder = baseService.getSortOrder();
        $scope.reload();
    }

    $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
    $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
    $scope.getNextPage = function () {  $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
    $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
    $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
    $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
    $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
    //page system

    $scope.ShowDevice = function () {
        $(".DisplayDevice").show();
        $(".SearchDevice").hide();
        $(".addDevice").hide(); 
        $scope.reload();
    }

    $scope.ShowSearch = function () {
        $(".DisplayDevice").hide();
        $(".SearchDevice").show();
        $(".addDevice").hide();
    }

    $scope.LoadSearch = function () {
        $scope.ShowDevice();
       
    }
	
	$scope.AddNewDevice = function () {
         $scope.resetModel (); 
		 $(".require").hide();
        $(".DisplayDevice").hide();
        $(".SearchDevice").hide(); 
        $(".addDevice").show();
 
    }
	
	$scope.onEditTagClick = function (item) { 
        $scope.AddNewDevice();   
		$scope.loadEditData(item);
    }
	
    $scope.loadEditData = function (item) {
        $scope.CreateModel = angular.copy(item); 
	}
	
	$scope.resetModel = function () {

        //$scope.CreateModel = { id:0, code:"", name : "", contact: "", address1 : "", address2: "", address3:"", tel:"", email:"", taxid: "", webcourse: ""};
		$scope.CreateModel = { id:0, course_name : "", course_type :1, duration:0};
    }
	
	
    $scope.resetSearch = function () {
        $scope.modelSearch = {
							"course_type" : ""
						}; 
		$scope.LoadSearch();
    }

    ////////////////////////////////////////////////////////////////////////////////////////
    // Event
    $scope.onInit = function () {   
		
		$scope.resetModel();
		$scope.resetSearch();
		

        $scope.listPageSize.forEach(function (entry, index) {
            if (0 === index)
                $scope.TempPageSize.selected = entry;
		});
		
		courseTypeApiService.getComboBox(null, function (results) {  
			console.log(results);
			if(true==results.status)
			{
				$scope.listcourseType = results.message
				if($scope.listcourseType.length > 0){
				   $scope.TempCourseTypeIndex.selected = $scope.listcourseType[0];
				   $scope.CreateModel.course_type = parseFloat($scope.TempCourseTypeIndex.selected.course_type);
				}
				else{
					baseService.showMessage(results.message);
				}
		
			}		
		})
		//$scope.reload();
    }
	
	$scope.reload = function (){ //ใช้สำหรับเรียก ข้อมูลจาก server (database) 
		courseApiService.listcourse($scope.modelSearch, function (results) {  
			var result= results.data; 
			if(result.status === true){  
				
				$scope.totalPage = result.toTalPage;  
				$scope.listPageIndex = baseService.getListPage(result.toTalPage);
				$scope.listPageIndex.forEach(function (entry, index) {
					if ($scope.PageIndex === entry.Value)
						$scope.TempPageIndex.selected = entry;
				});
				
				$scope.totalRecords =  result.totalRecords;
				$scope.modelDeviceList = result.message; 
			}else{
				
			}
		})
		// courseTypeApiService.listcourseType($scope.modelSearch, function (results) {  
		// 	var result= results.data; 
		// 	if(result.status === true){  
				
		// 		$scope.totalPage = result.toTalPage;  
		// 		$scope.listPageIndex = baseService.getListPage(result.toTalPage);
		// 		$scope.listPageIndex.forEach(function (entry, index) {
		// 			if ($scope.PageIndex === entry.Value)
		// 				$scope.TempPageIndex.selected = entry;
		// 		});
				
		// 		$scope.totalRecords =  result.totalRecords;
		// 		$scope.modelDeviceList = result.message; 
		// 	}else{
				
		// 	}
		// })
    }
 
    $scope.onDeleteTagClick = function(item){
		courseApiService.deletecourse({id:item.id}, function (result) {  
                if(result.status === true){  
					$scope.reload(); 
				}else{
					baseService.showMessage(result.message);
				}
            });
		
	}
	
	$scope.validatecheck = function(){
		var bResult = true; 
		$(".require").hide();
		 
		if($scope.CreateModel.coursename == ""){
			$(".CreateModel_coursename").show();
			bResult = false;
		}
		
		if($scope.CreateModel.description == ""){
			$(".CreateModel_description").show();
			bResult = false;
		}
		
		// if($scope.CreateModel.contact == ""){
			// $(".CreateModel_contact").show();
			// bResult = false;
		// }
		
		// if($scope.CreateModel.taxid == ""){
			// $(".CreateModel_taxid").show();
			// bResult = false;
		// }
		// if($scope.CreateModel.tel == ""){
			// $(".CreateModel_tel").show();
			// bResult = false;
		// }
		// if($scope.CreateModel.email == ""){
			// $(".CreateModel_email").show();
			// bResult = false;
		// }
		// if($scope.CreateModel.webcourse == ""){
			// $(".CreateModel_webcourse").show();
			// bResult = false;
		// }
		
		// if($scope.CreateModel.address1 == ""){
			// $(".CreateModel_address1").show();
			// bResult = false;
		// }
		// if($scope.CreateModel.address2 == ""){
			// $(".CreateModel_address2").show();
			// bResult = false;
		// }
		// if($scope.CreateModel.address3 == ""){
			// $(".CreateModel_address3").show();
			// bResult = false;
		// }
		
		
		
		// if($scope.CreateModel.price == "" && $scope.CreateModel.price != 0){
			// $(".CreateModel_price").show();
			// bResult = false;
		// }
	 
		// if($scope.CreateModel.note == ""){
			// $(".CreateModel_note").show();
			// bResult = false;
		// }
		
		return bResult;
	}
 
	$scope.onSaveTagClick = function () {
		
		
        var bValid = true;//$scope.validatecheck();
	  
		if(true == bValid){
            //console.log($scope.CriteriaModel);
            courseApiService.savecourse($scope.CreateModel, function (result) { 
				if(result.status == true){ 
					$scope.ShowDevice();  
				}else{
					baseService.showMessage(result.message);
				} 
            });

        }
		
    }


}]); 