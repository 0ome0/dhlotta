'use strict';
myApp.factory('courseApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Course/");
    return {
          
        //course 
	    listcourse: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCourseModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getcourse: function (model, onComplete) {
            baseService.postObject(servicebase + 'getcourseModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savecourse: function (model, onComplete) {
            baseService.postObject(servicebase + 'addcourse', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletecourse: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletecourse', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getcourseComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);