'use strict';
myApp.factory('classroomApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Classroom/");
    return {
          
        //class 
	    listclass: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getClassModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getcourse: function (model, onComplete) {
            baseService.postObject(servicebase + 'getcourseModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveclass: function (model, onComplete) {
            baseService.postObject(servicebase + 'addclassroom', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletecourse: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletecourse', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getcourseComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);