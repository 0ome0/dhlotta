'use strict';
myApp.factory('siteApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Site/");
    return {
          
        //site 
	    listsite: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getSiteModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getsite: function (model, onComplete) {
            baseService.postObject(servicebase + 'getSiteModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savesite: function (model, onComplete) {
            baseService.postObject(servicebase + 'addSite', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletesite: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteSite', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getSiteComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);