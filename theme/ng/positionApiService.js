'use strict';
myApp.factory('positionApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Position/");
    return {
          
        //position 
	    listposition: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPositionModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getposition: function (model, onComplete) {
            baseService.postObject(servicebase + 'getPositionModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveposition: function (model, onComplete) {
            baseService.postObject(servicebase + 'addPosition', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteposition: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletePosition', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPositionComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);