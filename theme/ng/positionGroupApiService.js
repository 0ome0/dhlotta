'use strict';
myApp.factory('positionGroupApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("PositionGroup/");
    return {
          
        //PositionGroup 
	    listpositionGroup: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPositionGroupModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getpositionGroup: function (model, onComplete) {
            baseService.postObject(servicebase + 'getPositionGroupModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savepositionGroup: function (model, onComplete) {
            baseService.postObject(servicebase + 'addPositionGroup', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletepositionGroup: function (model, onComplete) {
            baseService.postObject(servicebase + 'deletePositionGroup', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getPositionGroupComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);