'use strict';
myApp.factory('employeeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Employee/");
    return {
          
        //employee 
        listemployee: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getEmployeeModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getemployee: function (model, onComplete) {
            baseService.postObject(servicebase + 'getEmployeeModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveemployee: function (model, onComplete) {
            baseService.postObject(servicebase + 'addEmployee', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteemployee: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteEmployee', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getPositionComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);