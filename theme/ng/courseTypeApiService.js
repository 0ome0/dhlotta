'use strict';
myApp.factory('courseTypeApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("CourseType/");
    return {
          
        //CourseType
	    listcourseType: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getCourseTypeModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getcourseType: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCourseTypeModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        savecourseType: function (model, onComplete) {
            baseService.postObject(servicebase + 'addCourseType', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deletecourseType: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteCourseType', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.postObject(servicebase + 'getCourseTypeComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);