'use strict';
myApp.factory('invoiceApiService', ['$http', '$resource', 'baseService', function ($http, $resource, baseService) {
    var servicebase = get_base_url("Invoice/");
    return {
          
        //invoice 
	    listinvoice: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getInvoiceModelList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getinvoice: function (model, onComplete) {
            baseService.postObject(servicebase + 'getInvoiceModel', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        saveinvoice: function (model, onComplete) {
            baseService.postObject(servicebase + 'addInvoice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        deleteinvoice: function (model, onComplete) {
            baseService.postObject(servicebase + 'deleteInvoice', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
		getComboBox: function (model, onComplete) {
            baseService.searchObject(servicebase + 'getInvoiceComboList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
	    listinvoiceDetail: function (model, onComplete) {
            baseService.postObject(servicebase + 'getInvoiceDetailList', model, function (result) {
                if (undefined != onComplete) onComplete.call(this, result);
            });
        },
        
    };
}]);